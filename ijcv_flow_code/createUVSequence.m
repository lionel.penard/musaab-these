function uvSeq = createUVSequence(N)
%This function is used to compute SGSD over an entire sequence where N is
%the number of images, an initialization with HS algorithm is realised
%first.
image = imread('data\Arc-river\212.jpg');
[w h] = size(image);
uv = zeros([w h 2]);
 
for i=1: N-1
    x1 = imread(strcat('data\Arc-river\',num2str(i+211),'.jpg'));
    x2 = imread(strcat('data\Arc-river\',num2str(i+212),'.jpg'));
    if i == 1
        
  uv = estimate_flow_interface(x1, x2,uv,'hs');
  uv = estimate_flow_interface(x1, x2,uv,'sgsd');
    else
      uv = estimate_flow_interface(x1, x2,uv,'sgsd');  
    end
    
    uvSeq(:,:,:,i)=uv;
    i
end
