function d = Interpol(pos,t,i,seq)
if rem(t,1) == 0
    d = interp2(seq(:,:,1,i),pos(1),pos(2));
else  
    [rows,cols,~,slices] = size(seq);
    [X,Y,Z] = meshgrid(1:cols, 1:rows, 1:slices);
    d = interp3(X,Y,Z,squeeze(seq),pos(1),pos(2),t);
%     d = interp2(newFrame,pos(1),pos(2));
end
end