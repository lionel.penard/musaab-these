% It calculates ODE using Runge-Kutta 4th order method
% Author Ido Schwartz

function [x y] = Runge_Kutta_4(x_Init,y_Init,uvSeq)

% clc;                                               % Clears the screen
% clear all;
[rows,cols,~,slices] = size(uvSeq);
h=1;                                             % step size

% [w m n] = size(u);                                %calculate up to n
N = 1:h:slices; 
                                            
DeltaT = 1; 
tZero = 1;

x = zeros(1,length(N)); 
x(1) = x_Init;                                        % initial condition for u
y = zeros(1,length(N)); 
y(1) = y_Init;




for i=1:((length(N)-1))                             % calculation loop
    
    du_1 = Interpol([x(i) y(i)],tZero + N(i)*DeltaT,i,uvSeq(:,:,1,:));
    du_2 = Interpol([x(i)+0.5*du_1 y(i)+0.5*du_1],tZero+N(i)*DeltaT+0.5*DeltaT,i,uvSeq(:,:,1,:));
    du_3 = Interpol([x(i)+0.5*du_2 y(i)+0.5*du_2],tZero+N(i)*DeltaT+0.5*DeltaT,i,uvSeq(:,:,1,:));
    du_4 = Interpol([x(i)+du_3 y(i)+du_3],tZero+N(i)*DeltaT+DeltaT,i,uvSeq(:,:,1,:));

    x(i+1) = x(i) + (1/6)*(du_1+2*du_2+2*du_3+du_4);  % main equation
    
    dv_1 = Interpol([x(i) y(i)],tZero + N(i)*DeltaT,i,uvSeq(:,:,2,:));
    dv_2 = Interpol([x(i)+0.5*dv_1 y(i)+0.5*dv_1],tZero+N(i)*DeltaT+0.5*DeltaT,i,uvSeq(:,:,2,:));
    dv_3 = Interpol([x(i)+0.5*dv_2 y(i)+0.5*dv_2],tZero+N(i)*DeltaT+0.5*DeltaT,i,uvSeq(:,:,2,:));
    dv_4 = Interpol([x(i)+dv_3 y(i)+dv_3],tZero+N(i)*DeltaT+DeltaT,i,uvSeq(:,:,2,:));

    y(i+1) = y(i) + (1/6)*(dv_1+2*dv_2+2*dv_3+dv_4);  % main equation
    
end
end