This is optical flow code used during the thesis, it is based on Deging Sun matlab implementation (see his readme.pdf file).
For our purpose, the code now only contains two optical flow models, namely HS and SGSD. HS is used to initialize SGSD for the first pair of images.

You may start by checking CreateUVSequence.m file, it computes SGSD optical flow for the sequence example Arc found in data folder.

parameters to play with:
1. Turbulent Schmidt number Sct in FlowOperator.m.
2. The smoothing factor ope.lambda and ope.lambda_q in load-of-method.m under SGSD Switch-case. Note the latter is for the quadratic objective functions during GNC iterations.
3. The choice of which robust function to use is also in the same file/section as above under the variable "method".
4. Many other optical flow parameters like number of GNC iterations, number of pyramids and linearization steps etc could be found in sgsd.m file.
5. comment/uncomment this snippet of code in compute_flow_base.m to do median filtering or not between pyramids:

         if ~isempty(this.median_filter_size)
             uv0(:,:,1) = medfilt2(uv0(:,:,1), this.median_filter_size, 'symmetric');
             uv0(:,:,2) = medfilt2(uv0(:,:,2), this.median_filter_size, 'symmetric');
         end;
		 
6. Do the same as the above if you want to control the median filtering between GNC iterations, this time in the last lines of code in compute_flow.m file.


%%%%%%%%%%%%%%%%%%%%%%%

Use the file CreateTrajectories.m to create trajectories starting from one point in the image.

To create manual trajectories, use CellTracker_v1_1 code. The code found here is modified a bit to add a zooming function to more accurately track particles.