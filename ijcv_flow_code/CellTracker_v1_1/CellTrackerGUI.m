function varargout = CellTrackerGUI(varargin)
% CELLTRACKERGUI MATLAB code for CellTrackerGUI.fig
%      CELLTRACKERGUI, by itself, creates a new CELLTRACKERGUI or raises the existing
%      singleton*.
%
%      H = CELLTRACKERGUI returns the handle to a new CELLTRACKERGUI or the handle to
%      the existing singleton*.
%
%      CELLTRACKERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERGUI.M with the given input arguments.
%
%      CELLTRACKERGUI('Property','Value',...) creates a new CELLTRACKERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTrackerGUI

% Last Modified by GUIDE v2.5 30-Jun-2015 15:29:34

% Added by Filippo
addpath(['.' filesep 'functions'])

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CellTrackerGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CellTrackerGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% CELLTRACKER HEADER FUNCTIONS


% --- Executes just before CellTrackerGUI is made visible.
function CellTrackerGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerGUI (see VARARGIN)

% Choose default command line output for CellTrackerGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Added by Filippo
warning('off')
path = fullfile(filepartsCustom(mfilename([pwd filesep 'functions' filesep])), 'loci_tools.jar');
javaaddpath(path);
warning('on')

clearvars -global f_handles
global f_handles

% Settings for handlers (by Filippo)
f_handles.firstImage = 1;
f_handles.lastImage = 1;
f_handles.h_FCpb01 = handles.FCpb01;
f_handles.h_FCpb02 = handles.FCpb02;
f_handles.h_FCpb03 = handles.FCpb03;
f_handles.h_FCpb04 = handles.FCpb04;
f_handles.h_FCpb05 = handles.FCpb05;
f_handles.h_FCpb06 = handles.FCpb06;
f_handles.h_FCpb07 = handles.FCpb07;
f_handles.h_GUImain = gcf;
f_handles.h_GUIsetG = [];
f_handles.h_GUIsetT = [];
f_handles.h_GUIsetS = [];
f_handles.h_GUIsetM = [];
f_handles.h_GUIshow = [];
f_handles.h_GUIstatistics = [];
f_handles.pos = [];
f_handles.PathImgOriginal = [];
f_handles.PathImgOriginalReal = [];
f_handles.PathImgCurrent = [];
f_handles.lastIndexShown = 1;
f_handles.showImageID = 1;

% default parameters shown in the figure "CellTrackerSettings"

%background correction
f_handles.backCorrMethod = 1; % Bicubic interpolation
f_handles.backCorrSize = 100; % 100 pixels average

%aligner
f_handles.maxAlignDistance = 100;

%tracking general
f_handles.CellMatchingModality = 0; % 0=template matching; 1=histogram matching
f_handles.CellMaxDisp    = 20;
f_handles.CellTempSize   = 40;
f_handles.TrackFirstPoint = [];
f_handles.TrackLastPoint = [];
f_handles.TrackStepPoint = [];

%automatic tracking
f_handles.templateTreshold = 3;
f_handles.tracking.mem = 5;
f_handles.tracking.minlength = 30;
f_handles.tracking.stepsize = 12;

%semi-automatic tracking
f_handles.manBackSearch = 0;

%background correction
f_handles.DynamicInterpolation = 0; % 1 = dynamic; alse = linear interpolation
f_handles.TrackStepPoint = 1;

if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
% create tmp folder
if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;

% UIWAIT makes CellTrackerGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles

% Get default command line output from handles structure
varargout{1} = handles.output;

% Set bottom
guidata(hObject, handles);
set(f_handles.h_FCpb01, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb02, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb03, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb04, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb05, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb06, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb07, 'BackgroundColor', 'green');
drawnow
set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
drawnow
set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
drawnow
set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
drawnow
set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
drawnow
set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
drawnow
set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
drawnow
set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
drawnow


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc, clear, close all


% --- Executes on button press in AboutUs.
function AboutUs_Callback(hObject, eventdata, handles)
% hObject    handle to AboutUs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'SOFTWARE VERSION', ...
    'CellTracker v1.0', ...
    'July, 2015', ...
    ' ', ...
    'REQUIREMENTS', ...
    'MATLAB 2009b and Image Processing Toolbox 8.1 or later versions.', ...
    ' ', ...
    'CONTACTS', ...
    'Peter Horvath, Ph.D.', ...
    'Synthetic and Systems Biology Unit', ...
    'Hungarian Academia of Sciences, BRC, Szeged', ...
    'horvath.peter@brc.mta.hu', ...
    ' ', ...
    'LICENSE', ...
    'Copyright (c) 2015, Peter Horvath and Filippo Piccinini, All rights reserved.', ... 
    'This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.', ...
    'This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.', ...
    'You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.', ...
    ' ', ...
    ' '};
msgbox(Message,'About us')


% --- Executes on button press in Logo.
function Logo_Callback(hObject, eventdata, handles)
% hObject    handle to Logo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function Logo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Logo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'Logo_withCT_withTransparency_noBorder_h80x141px.png']);
% RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', bg_image);

% Update handles structure
guidata(hObject, handles);


%% FLOW CHART: "LOAD AN INPUT FILE" FUNCTIONS


% --- Executes on button press in FCpb01.
function FCpb01_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Open an image file
global f_handles

% create tmp folder
if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
f_handles.pos = [];
 
 try
    if f_handles.PathImgOriginalReal ==0; f_handles.PathImgOriginalReal = []; end;
    [filename, pathname, filterindex] = uigetfile({'*.tif;*.tiff','Stack images (*.tif, *.tiff)'; ...
        '*.avi','Video files (*.avi)'; ...
        '*.*', 'Bio-Formats images (*.*)'}, 'Open a file', 'MultiSelect', 'off', ...
        f_handles.PathImgOriginalReal);
    f_handles.PathImgOriginalReal = pathname;
    
    if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
    f_handles.pos = [];
    
    if ischar(filename) && ischar(pathname) && filterindex ~= 0
        if ishandle(f_handles.h_GUIshow); close(f_handles.h_GUIshow); end;
        if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;

        contents = [pathname filename];
        [pathstr, name, ext] = fileparts(contents);
        if strcmp(ext, '.tiff') || strcmp(ext, '.tif')
            inputExtension = 1; % tif or tiff
        elseif strcmp(ext, '.avi')
            inputExtension = 2; % avi
        else
            inputExtension = 0; % Bioformat
        end

        if inputExtension == 0
            % Conversion by using LOCI
            newImageName = [pwd filesep 'tmpct' filesep name '.tif'];
            
            % Functions to read BioFormats
            % load the Bio-Formats library into the MATLAB environment
            % Problem: if you leave here this code the global variables are
            % deleted. Put this at the first part of CellTracker_OpeningFcn
            %javaaddpath([pwd filesep 'functions' filesep 'loci_tools.jar']); 
            
            % check if there are non tiff images and there is a need to convert them using LOCI
            [lociImgPointer, lociImageProps] = lociLoadHeader(contents);

            % load the first image and save
            lociImage = lociLoadImage(lociImgPointer, lociImageProps, 1);
            if size(lociImage,3)>1
                lociImage = rgb2gray(lociImage);
            end
            imwrite(lociImage, newImageName);

            % append the other images
            for j=2:lociImageProps.numImages
                lociImage = lociLoadImage(lociImgPointer, lociImageProps, j);
                if size(lociImage,3)>1
                    lociImage = rgb2gray(lociImage);
                end
                imwrite(lociImage, newImageName, 'WriteMode', 'append');
            end
            
            % change the image path to the tmp
            contents = newImageName;
            
        elseif inputExtension == 2
            % Conversion from avi to stack of tif
            newImageName = [pwd filesep 'tmpct' filesep name '.tif'];

            % Read in the movie.
            try
                h_wait = waitbar(0, 'Please wait...');
                mov = aviread(contents);
                %obj = mmreader(contents); mov = read(obj);
                %obj = VideoReader(contents); mov = read(obj);
                % Determine how many frames there are.
                numberOfFrames = size(mov, 2);
                %numberOfFrames = mov.NumberOfFrames;
            catch ME1
                if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
                Message = {'Impossible reading this imput file for this problem: ', ...
                    ' ', ...
                    ME1.message, ...
                    ' ', ...
                    'How to solve the problem? Open the original video with ImageJ, Save As tif, then open with CellTracker the tif file generated by ImageJ.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            
            % Create stack of tif
            for f = 1:numberOfFrames
                % Extract the frame from the movie structure.
                thisFrame = mov(f).cdata;
                %thisFrame = mov(:,:,:,f);
                
                if size(thisFrame,3)>1
                    thisFrame = rgb2gray(thisFrame);
                end
                
                % Write it out to disk.
                if f == 1
                    imwrite(thisFrame, newImageName); 
                    oldFrame = thisFrame;
                else
                    imageDifferenza = abs(thisFrame-oldFrame);
                    if max(imageDifferenza)>=0.01 % To delete frame repeated due to avi file creation
                        imwrite(thisFrame, newImageName, 'WriteMode', 'append');
                        oldFrame = thisFrame;
                    end
                    clear imageDifferenza
                end
                waitbar(f/numberOfFrames)
                clear thisFrame
            end
            if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
            
            % change the image path to the tmp
            contents = newImageName;
            
        else
            % The input file is a stack of tif
            
            % rgb2gray conversion
            stopIm  = length(imfinfo(contents));
            inputImage = imread(contents, 1);
            if size(inputImage,3)>1
                newImageName = [pwd filesep 'tmpct' filesep name '.tif'];
                for f = 1:stopIm
                    inputImage = imread(contents, f);
                    inputImage = rgb2gray(inputImage);
                    if f == 1
                        imwrite(inputImage, newImageName); 
                    else
                        imwrite(inputImage, newImageName, 'WriteMode', 'append');
                    end
                end
            end
        end
          
        f_handles.PathImgOriginal = contents;
        f_handles.PathImgCurrent = f_handles.PathImgOriginal;

        % Update the image window    
        if ~isempty(f_handles.PathImgOriginal)
            f_handles.startIm = 1;
            f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
            if f_handles.lastImage==1
                Message = {'The input file must contain at least two images to perform tracking.', ...
                    ' '};
                msgbox(Message,'Message')
            else       
                f_handles.h_GUIshow = CellTrackerFigureShow;
                guidata(hObject, handles);
                updateSliceWindows(f_handles.startIm, f_handles.PathImgOriginal, handles);
                set(f_handles.h_FCpb01, 'BackgroundColor', 'green');
                set(f_handles.h_FCpb02, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb03, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
                if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            end
        else
            Message = {'Empty file list!', ...
            ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
    
catch ME1
    if ishandle(f_handles.h_GUIshow); close(f_handles.h_GUIshow); end;
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    Message = {'Could not perform operation (open and/or conversion).', ...
        ' '};
    msgbox(Message,'Message')
    set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
    set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    % create tmp folder
    if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
    f_handles.pos = [];
end


% --- Executes during object creation, after setting all properties.
function pb_cropt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_cropt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Choose default command line output for bg_image

% Choose default command line output for bg_image
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'cut-t2.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_cropt.
function pb_cropt_Callback(hObject, eventdata, handles)
% hObject    handle to pb_cropt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles

% crop time
try
    f_handles.pos = [];
    
    colore = get(f_handles.h_FCpb01, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    elseif isequal(colore,[1,1,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else      
        
        % Resent input
        f_handles.PathImgCurrent = f_handles.PathImgOriginal;
        f_handles.startIm = 1;
        f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
        lastIm =  f_handles.lastImage;
        
        % set input image name
        currentImageName = char(f_handles.PathImgOriginal);
        [pathstr, fileName, ext] = fileparts(currentImageName);
        outFile = [pwd filesep 'tmpct' filesep fileName '_crTime.tif'];

        % dialog
        prompt = {'First image:','Last image:'};
        dlg_title = 'Time crop parameters';
        num_lines = 1;
        def = {'1', num2str(lastIm)};
        answer = inputdlg(prompt,dlg_title,num_lines,def,'on');

        % Errors check
        flag_ComputationOn = 1;
        if ~isempty(answer)
            TestStartIm = str2num(answer{1});
            TestStopIm = str2num(answer{2});
        else
            flag_ComputationOn = 0;
            TestStartIm = 1;
            TestStopIm = lastIm;
        end
        if TestStartIm>TestStopIm || TestStartIm>lastIm || TestStartIm<1
            flag_ComputationOn = 0;
            startIm = 1;
        else
            startIm = TestStartIm;
        end
        if TestStopIm<TestStartIm || TestStopIm>lastIm || TestStopIm<1
            flag_ComputationOn = 0;
            stopIm = lastIm;
        else
            stopIm = TestStopIm;
        end
        if startIm==stopIm
            flag_ComputationOn = 0;
        end

        % Computation
        if flag_ComputationOn==1
            h_wait = waitbar(0, 'Please wait...');
            for i=startIm:stopIm        
                % read in
                in = imread(currentImageName, i);
                % save
                if i==startIm
                    imwrite(in, outFile)
                else
                    imwrite(in, outFile, 'WriteMode', 'append');
                end
            waitbar(i/stopIm)
            end
            if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;

            % update file list
            f_handles.PathImgOriginal = outFile;
            f_handles.PathImgCurrent = f_handles.PathImgOriginal;

            % Update the image window   
            if ~isempty(f_handles.PathImgOriginal)
                f_handles.PathImgCurrent = char(f_handles.PathImgOriginal);
                f_handles.startIm = 1;
                f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
                guidata(hObject, handles);  
                updateSliceWindows(f_handles.startIm, f_handles.PathImgOriginal, handles); 
                set(f_handles.h_FCpb02, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb03, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
                set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
                if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            else
                Message = {'Empty file list!', ...
                    ' '};
                msgbox(Message,'Message')
            end
        else
            Message = {'Wrong input parameters: time-crop could not perform.', ...
                ' '};
            msgbox(Message,'Message')
        end
    end   
catch ME1
    if ishandle(f_handles.h_GUIshow); close(f_handles.h_GUIshow); end;
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    Message = {'No image is open: time crop could not perform.', ...
        ' '};
    msgbox(Message,'Message')
    set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
    set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    % create tmp folder
    if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
    f_handles.pos = [];
end


% --- Executes during object creation, after setting all properties.
function pb_cropxy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_cropxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Choose default command line output for bg_image
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'cut-xy2.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_cropxy.
function pb_cropxy_Callback(hObject, eventdata, handles)
% hObject    handle to pb_cropxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles

% crop X-Y
try
    f_handles.pos = [];
    
    colore = get(f_handles.h_FCpb01, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    elseif isequal(colore,[1,1,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        % Reset input
        f_handles.PathImgCurrent = f_handles.PathImgOriginal;
        f_handles.startIm = 1;
        f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
        guidata(hObject, handles);
        updateSliceWindows(f_handles.startIm, f_handles.PathImgOriginal, handles);
        
        inpImage = imread(f_handles.PathImgCurrent, 1);
        [rowI, colI, chI] = size(inpImage);
        clear inpImage
        
        % set input image name
        currentImageName = char(f_handles.PathImgOriginal);
        
        Message = {'Draw a rectangle to select the region you want to keep.', ...
            ' ', ...   
            'Close this message window to proceed.', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);

        % select rectangle
        %rect = uint16(getrect(f_handles.h_axes1));
        try
            rect = uint16(getrect(f_handles.h_axes1));
        catch ME1
            msgbox(ME1.message)
        end
       
        % check for errors
        if rect(3)==0 && rect(4)==0;
            Message = {'Draw a rectangle.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        rect(rect<1) = 1;
        if rect(1) > colI || rect(1) > colI ;
            Message = {'Draw a rectangle.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        if rect(3) > colI-rect(1);
            rect(3) = colI-rect(1);
        end
        if rect(4) > rowI-rect(2);
            rect(4) = rowI-rect(2);
        end
        if rect(1)==1 && rect(2)==1 && rect(3)==colI-1 && rect(4)==rowI-1 
            return
        end
        
        [pathstr, fileName, ext] = fileparts(currentImageName);
        outFile = [pwd filesep 'tmpct' filesep fileName '_crXY.tif'];
        lastIm =  length(imfinfo(currentImageName));

        h_wait = waitbar(0, 'Please wait...');
        for i=1:lastIm        
            % read in
            in = imread(currentImageName, i);
            % crop
            in = in(rect(2):rect(2)+rect(4), rect(1):rect(1)+rect(3), :);
            % save
            if i==1
                imwrite(in, outFile)
            else
                imwrite(in, outFile, 'WriteMode', 'append');
            end
            waitbar(i/lastIm)
        end
        if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
        
        % update file list
        f_handles.PathImgOriginal = outFile;
        f_handles.PathImgCurrent = f_handles.PathImgOriginal;

        % Update the image window   
        if ~isempty(f_handles.PathImgOriginal)
            f_handles.PathImgCurrent = char(f_handles.PathImgOriginal);
            f_handles.startIm = 1;
            f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
            guidata(hObject, handles);
            updateSliceWindows(f_handles.startIm, f_handles.PathImgOriginal, handles);
            set(f_handles.h_FCpb02, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
        else
            Message = {'Empty file list!', ...
                ' '};
            msgbox(Message,'Message')
        end
    end
    
catch ME1
    Message = {'No image is open.', ...
        ' '};
    msgbox(Message,'Message')
    set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
    set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
    set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    % create tmp folder
    if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
    f_handles.pos = [];
end


%% FLOW CHART: "VIGNETTIN CORRECTION" FUNCTIONS


% --- Executes on button press in FCpb02.
function FCpb02_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Background Correction

global f_handles

% check if the filelist is empty
try
    f_handles.pos = [];
    
    colore = get(f_handles.h_FCpb02, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    elseif isequal(colore,[0,1,0]);
        Message = {'Computation already performed.', ...
            ' '};
        msgbox(Message,'Message')
    else     
        if ~isempty(f_handles.PathImgCurrent)
            f_handles.startIm = 1;        
            f_handles.stopIm  = length(imfinfo(char(f_handles.PathImgCurrent)));
            [pathstr, fileName, ext] = fileparts(char(f_handles.PathImgCurrent));
            
            try
                backgroundCorrection(char(f_handles.PathImgCurrent), f_handles.startIm, f_handles.stopIm, f_handles.backCorrSize, f_handles.backCorrMethod);
            catch ME1
                if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end; 

                Message = {'Could not perform this vignetting correction. Try to change correction method.', ...
                    ' '};
                msgbox(Message,'Message')
                return;
            end
                
            f_handles.PathImgCurrent = [pwd filesep 'tmpct' filesep fileName '_bc.tif'];
            fileName = [fileName '_bc'];

            guidata(hObject, handles);
            updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
            set(f_handles.h_FCpb02, 'BackgroundColor', 'green');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
        else
            Message = {'No image is open: background correction could not perform.', ...
                ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
catch ME1
    Message = {'Sorry, the background correction failed.', ...
        ' '};
    msgbox(Message,'Message')
end


%% FLOW CHART: "AUTOMATIC ALIGNER" FUNCTIONS


% --- Executes on button press in FCpb03.
function FCpb03_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Automatic Aligner

global f_handles

% check if the filelist is empty
try
    f_handles.pos = [];
    colore = get(f_handles.h_FCpb03, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    elseif isequal(colore,[0,1,0]);
        Message = {'Computation already performed.', ...
            ' '};
        msgbox(Message,'Message')
    else   
        if ~isempty(f_handles.PathImgCurrent)
            f_handles.startIm = 1;        
            f_handles.stopIm  = length(imfinfo(char(f_handles.PathImgCurrent)));
            [pathstr, fileName, ext] = fileparts(char(f_handles.PathImgCurrent));
            
            transList = stackAligner(f_handles.PathImgCurrent, f_handles.startIm, f_handles.stopIm);
            
            f_handles.PathImgCurrent = [pwd filesep 'tmpct' filesep fileName '_al.tif'];
            fileName = [fileName '_al'];

            guidata(hObject, handles);
            updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
            set(f_handles.h_FCpb03, 'BackgroundColor', 'green');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
        else          
            Message = {'No image is open: image aligner could not perform.', ...
                ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
catch ME1
    Message = {'Sorry, the image aligner failed.', ...
        ' '};
    msgbox(Message,'Message')
end


%% FLOW CHART: "AUTOMATIC TRACKER" FUNCTIONS


% --- Executes on button press in FCpb04.
function FCpb04_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% AUTOMATIC TRACKER
global f_handles

% Check if there are images open.
try
    colore = get(f_handles.h_FCpb04, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        % If there are no images open.
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        % If there are images open.
        if ~isempty(f_handles.PathImgOriginal)       
            try
                % Open a new GUI to set the parameters         
                f_handles.flagGUIclosedWithX = 0;
                f_handles.h_GUIsetT = CellTrackerSettingsTracking;
                waitfor(f_handles.h_GUIsetT);
                if f_handles.flagGUIclosedWithX==1
                    return
                end
                
                % Define the object template (rectangle)
                guidata(hObject, handles);
                updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
                axes(f_handles.h_axes1);
                %rect = getrect();
                try
                    rect = getrect();
                catch ME1
                    msgbox(ME1.message)
                end
                
                if rect(3)==0 && rect(4)==0
                    Message = {'The choosen template can not be used.', ...
                        ' '};
                    msgbox(Message,'Message')
                    return
                end
                tempImage = imread(f_handles.PathImgCurrent, f_handles.imageIndex);
                tempImage = tempImage(round(rect(2)):round(rect(2)+rect(4)), round(rect(1)):round(rect(1)+rect(3)));
                if ~isempty(tempImage)
                    if isequal(exist([pwd filesep 'tmpct' filesep 'temp.tif'], 'file'),2); delete([pwd filesep 'tmpct' filesep 'temp.tif']); imwrite(tempImage, [pwd filesep 'tmpct' filesep 'temp.tif']); else imwrite(tempImage, [pwd filesep 'tmpct' filesep 'temp.tif']); end;
                end
            catch ME1
                Message = {'The choosen template can not be used.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            
            f_handles.startIm = 1;        
            f_handles.stopIm  = length(imfinfo(char(f_handles.PathImgCurrent)));       
            f_handles.CellMatchingModality = 0; % ATTENTION: We impose TemplateMatching because HistogramMatching here is very slow!
            
            %THE USERS CAN CHANGE THIS FUNCTION TO MODIFY THE ALGORITHM TO FIND THE CELLS
            cellpos = cellDetector(f_handles.PathImgCurrent, f_handles.startIm, f_handles.stopIm, f_handles.templateTreshold, f_handles.CellMatchingModality);
            cellpos = sortrows(cellpos, 3); %To sort the matrix according to the last column.
            
            % Tracking parameters
            param.mem = f_handles.tracking.mem;
            param.dim = 2;
            param.good = f_handles.tracking.minlength;
            param.quiet = 1; 
            try
                warning('off')
                CellMaxDisp = f_handles.tracking.stepsize;
                outpos = track(cellpos, CellMaxDisp, param);
                clear CellMaxDisp
                warning('on')
            catch ME1
                Message = {'Sorry, the tracker failed.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            
            % To update track list
            if ~isempty(f_handles.pos)
                pos = f_handles.pos;
                maxidx = max(pos(:,4));
                outpos(:, 4) = outpos(:, 4) + maxidx;
                f_handles.pos = [pos' outpos']';
                clear pos maxidx
            else
                f_handles.pos = outpos;
            end
            
            % Save output
            path = [pwd filesep 'tmpct' filesep];
            fileNameMat = 'TracksCoordinates.mat';
            fileNameExcel = 'TracksCoordinates.xls';
            pathFileNameMat = [path fileNameMat];
            pathFileNameExcel = [path fileNameExcel];
            if ~isempty(f_handles.pos)
                pos = f_handles.pos;
                if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
                if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
            else
                if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
                if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
            end

            % Update visualization
            guidata(hObject, handles);
            updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
            set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'yellow');
        else
            Message = {'No image is open.', ...
                ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
catch ME1
    Message = {'Sorry, the tracker failed.', ...
        ' '};
    msgbox(Message,'Message')
    if isempty(f_handles.pos)
        set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
        if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    end
end


%% FLOW CHART: "semi-automatic CELL TRACKING" FUNCTIONS


% --- Executes on button press in FCpb05.
function FCpb05_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb05 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Multiple semi-automatic tracking

global f_handles

try    
    colore = get(f_handles.h_FCpb05, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        if ~isempty(f_handles.PathImgCurrent)

            f_handles.flagGUIclosedWithX = 0;
            f_handles.h_GUIsetS = CellTrackerSettingsSemi;
            waitfor(f_handles.h_GUIsetS);
            if f_handles.flagGUIclosedWithX==1
                return
            end
            
            % Errors check
            inpImage = imread(f_handles.PathImgOriginal, 1);
            [rowI, colI, chI] = size(inpImage);
            clear inpImage
            f_handles.lastImage = length(imfinfo(f_handles.PathImgOriginal));
            flag_ComputationOn = 1;
            if f_handles.TrackFirstPoint>f_handles.TrackLastPoint || f_handles.TrackFirstPoint>f_handles.lastImage || f_handles.TrackFirstPoint<1
                flag_ComputationOn = 0;
            end
            if f_handles.TrackLastPoint>f_handles.lastImage
                flag_ComputationOn = 0;
            end
            if f_handles.TrackLastPoint==f_handles.TrackFirstPoint
                flag_ComputationOn = 0;
                Message = {'The first and the last image cannot be the same.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            
            if flag_ComputationOn==0
                Message = {'Wrong input parameters.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            else
                guidata(hObject, handles);
                updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                axes(f_handles.h_axes1);
                
                % Multiple selection with cross shown
                inpImage = imread(f_handles.PathImgCurrent, f_handles.TrackFirstPoint);
                maxValue = max(inpImage(:));
                [rowI, colI, chI] = size(inpImage);
                x = []; y = [];
                xx = -1; yy = -1;
                while ~isempty(xx) && ~isempty(yy)
                    [xx, yy] = ginputc(1, 'Color', 'r');
                    if ~isempty(xx) && ~isempty(yy)
                        if xx>6 && xx<colI-6 && yy>6 && yy<rowI-6
                            xx = round(xx);
                            yy = round(yy);
                            x = [x; xx];
                            y = [y; yy];
                            inpImage(yy-6:yy+6, xx-2:xx+2) = maxValue-1;
                            inpImage(yy-2:yy+2, xx-6:xx+6) = maxValue-1;
                            name = 'CurrentImageWithCross';
                            newImageName = [pwd filesep 'tmpct' filesep name '.tif'];
                            imwrite(inpImage, newImageName);
                            updateSliceWindows(f_handles.TrackFirstPoint, newImageName, handles);
                            clear newImageName name
                        end
                    end
                end
                clear inpImage
                updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                numCells = length(x);
                
                h_wait = waitbar(0, 'Please wait...');
                for k = 1:numCells
                    if y(k)-f_handles.CellTempSize<1 || y(k)+f_handles.CellTempSize>rowI || x(k)-f_handles.CellTempSize<1 || x(k)+f_handles.CellTempSize>colI
                        Sentence = ['The ' num2str(k) '-th cell was too close to the border of the image. It was not tracked.'];
                        Message = {Sentence, ... 
                            ' '};
                        h_msg = msgbox(Message,'Message');
                    else
                        CellRadius = ceil(f_handles.CellTempSize/2); % This to work with radious and not diameter.
                        outpos = semiautomaticTracking(y(k), x(k), f_handles.TrackFirstPoint, f_handles.PathImgCurrent, f_handles.startIm, f_handles.TrackLastPoint, CellRadius, f_handles.CellMaxDisp, f_handles.manBackSearch, f_handles.CellMatchingModality);
                        if ~isempty(f_handles.pos)
                            maxidx = max(f_handles.pos(:,4));
                            outpos(:, 4) = outpos(:, 4) + maxidx;
                            f_handles.pos = [f_handles.pos' outpos']';
                        else
                            f_handles.pos = outpos;
                        end

                        % Save output
                        path = [pwd filesep 'tmpct' filesep];
                        fileNameMat = 'TracksCoordinates.mat';
                        fileNameExcel = 'TracksCoordinates.xls';
                        pathFileNameMat = [path fileNameMat];
                        pathFileNameExcel = [path fileNameExcel];
                        if ~isempty(f_handles.pos)
                            pos = f_handles.pos;
                            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
                            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
                        else
                            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
                            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
                        end

                        % Update visualization
                        guidata(hObject, handles);
                        updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                        waitbar(k/numCells)
                        set(f_handles.h_FCpb07, 'BackgroundColor', 'yellow');
                    end
                end
                if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
            end
        else
            Message = {'No image is open.', ...
                ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
catch ME1
    if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
    Message = {'Sorry, the tracker failed.', ...
        ' '};
    msgbox(Message,'Message')
    if isempty(f_handles.pos)
        set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
        if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    end
end


%% FLOW CHART: "MANUAL TRACKING" FUNCTIONS


% --- Executes on button press in FCpb06.
function FCpb06_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb06 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Single manual tracking by clicking

global f_handles

try    
    colore = get(f_handles.h_FCpb05, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        if ~isempty(f_handles.PathImgCurrent)
            
            f_handles.flagGUIclosedWithX = 0;
            f_handles.h_GUIsetM = CellTrackerSettingsManual;
            waitfor(f_handles.h_GUIsetM);
            if f_handles.flagGUIclosedWithX==1
                return
            end
            
            
            % Errors check
            inpImage = imread(f_handles.PathImgOriginal, 1);
            [rowI, colI, chI] = size(inpImage);
            clear inpImage
            f_handles.lastImage = length(imfinfo(f_handles.PathImgOriginal));
            flag_ComputationOn = 1;
            if f_handles.TrackFirstPoint>f_handles.TrackLastPoint || f_handles.TrackFirstPoint>f_handles.lastImage || f_handles.TrackFirstPoint<1
                flag_ComputationOn = 0;
            end
            if f_handles.TrackLastPoint>f_handles.lastImage
                flag_ComputationOn = 0;
            end
            if f_handles.TrackLastPoint==f_handles.TrackFirstPoint
                flag_ComputationOn = 0;
                Message = {'The first and the last image cannot be the same.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            if f_handles.TrackStepPoint>(f_handles.TrackLastPoint-f_handles.TrackFirstPoint) || f_handles.TrackStepPoint<1
                flag_ComputationOn = 0;
                Message = {'The gap between two subsequent clicks is too large.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
            
            if flag_ComputationOn==0
                Message = {'Wrong input parameters.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            else
                
                if f_handles.TrackStepPoint == 1
                    f_handles.DynamicInterpolation = 0;
                end

                if f_handles.DynamicInterpolation == 1
                    
                    % DYNAMIC TEMPLATE MATHCING
                    
                    guidata(hObject, handles);
                    updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                    NewTrack = [];
                    m = f_handles.TrackFirstPoint;
                    m_old = f_handles.TrackFirstPoint;
                    while m <= f_handles.TrackLastPoint

                        guidata(hObject, handles);
                        updateSliceWindows(m, f_handles.PathImgCurrent, handles);
                        axes(f_handles.h_axes1);
                        [xcol, yrow] = ginputc(1, 'Color', 'g');
                        if ~isempty(xcol) && ~isempty(yrow)
                            xcol = round(xcol);
                            yrow = round(yrow);
                            if yrow<1 || yrow>rowI || xcol<1 || xcol>colI 
                                Message = {'Wrong input selection.', ...
                                    ' '};
                                msgbox(Message,'Message')
                                return
                            end
                            
                            if m == f_handles.TrackFirstPoint
                                NewTrack = [yrow, xcol, m, 1];
                            else
                                oldyrow = NewTrack(end,1);
                                oldxcol = NewTrack(end,2);
                                oldtime = NewTrack(end,3); 
                                NewTrack2 = trackComputationDynamicApproach(oldyrow, oldxcol, oldtime, yrow, xcol, m);
                                if isempty(NewTrack2)
                                    Message = {'This cell can be tracked because is to close to the image border.', ...
                                        ' '};
                                    msgbox(Message,'Message')
                                    return
                                end
                                NewTrack = [NewTrack; NewTrack2];
                                clear NewTrack2
                            end
                            
                            m_old = m;
                            if m+f_handles.TrackStepPoint>f_handles.TrackLastPoint && m~=f_handles.TrackLastPoint
                                m = f_handles.TrackLastPoint;
                            else
                                m = m + f_handles.TrackStepPoint;
                            end
                        else
                            f_handles.TrackLastPoint = m_old;
                            m = m_old;
                            break
                        end
                    end
                    
                else
                    
                    % LINEAR INTERPOLATION
                    
                    guidata(hObject, handles);
                    updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                    NewTrack = [];
                    m = f_handles.TrackFirstPoint;
                    m_old = f_handles.TrackFirstPoint;
                    while m <= f_handles.TrackLastPoint

                        guidata(hObject, handles);
                        updateSliceWindows(m, f_handles.PathImgCurrent, handles);
                        axes(f_handles.h_axes1);
                        [xcol, yrow] = ginputc(1, 'Color', 'g');
                        if ~isempty(xcol) && ~isempty(yrow)
                            xcol = round(xcol);
                            yrow = round(yrow);
                            if yrow<1 || yrow>rowI || xcol<1 || xcol>colI 
                                Message = {'Wrong input selection.', ...
                                    ' '};
                                msgbox(Message,'Message')
                                return
                            end

                            if isempty(NewTrack)
                                NewTrack = [yrow, xcol, m, 1];
                            else
                                oldyrow = NewTrack(end,1);
                                oldxcol = NewTrack(end,2);
                                oldtime = NewTrack(end,3);
                                numMissingStep = m-oldtime;
                                for s = 1:numMissingStep
                                    speedyrow = (yrow-oldyrow)/numMissingStep;
                                    speedxcol = (xcol-oldxcol)/numMissingStep;
                                    NewTrack = [NewTrack; round(oldyrow+(s*speedyrow)), round(oldxcol+(s*speedxcol)), oldtime+s, 1];
                                end
                            end
                            m_old = m;
                            if m+f_handles.TrackStepPoint>f_handles.TrackLastPoint && m~=f_handles.TrackLastPoint
                                m = f_handles.TrackLastPoint;
                            else
                                m = m + f_handles.TrackStepPoint;
                            end
                        else
                            f_handles.TrackLastPoint = m_old;
                            m = m_old;
                            break
                        end
                    end

                end
                
                % Update tracks
                if ~isempty(NewTrack)
                    if ~isempty(f_handles.pos)
                        maxidx = max(f_handles.pos(:,4));
                        NewTrack(:, 4) = NewTrack(:, 4) + maxidx;
                        f_handles.pos = [f_handles.pos' NewTrack']';
                    else
                        f_handles.pos = NewTrack;
                    end
                    clear NewTrack
                end
                
                % Save output
                path = [pwd filesep 'tmpct' filesep];
                fileNameMat = 'TracksCoordinates.mat';
                fileNameExcel = 'TracksCoordinates.xls';
                pathFileNameMat = [path fileNameMat];
                pathFileNameExcel = [path fileNameExcel];
                if ~isempty(f_handles.pos)
                    pos = f_handles.pos;
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
                else
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
                end
                
                % Update visualization
                guidata(hObject, handles);
                updateSliceWindows(f_handles.TrackFirstPoint, f_handles.PathImgCurrent, handles);
                if ~isempty(f_handles.pos)
                    set(f_handles.h_FCpb07, 'BackgroundColor', 'yellow');
                end
            end
        else
            Message = {'No image is open.', ...
                ' '};
            msgbox(Message,'Message')
            set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
            set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
            set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
            % create tmp folder
            if isequal(exist('tmpct', 'dir'),7); rmdir('tmpct','s'); mkdir('tmpct'); else mkdir('tmpct'); end;
            f_handles.pos = [];
        end
    end
catch ME1
    Message = {'Sorry, the tracker failed.', ...
        ' '};
    msgbox(Message,'Message')
    if isempty(f_handles.pos)
        set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
        if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    end
end


%% FLOW CHART: "EDIT TRACKS" FUNCTIONS


% --- Executes during object creation, after setting all properties.
function pb_removeAllTrack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_removeAllTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'remAllPath.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_removeAllTrack.
function pb_removeAllTrack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_removeAllTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Delete all tracks
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else

        %Save in tmp folder the current tracks
        if ~isempty(f_handles.pos)
            path = [pwd filesep 'tmpct' filesep];
            fileNameMat = 'TracksCoordinates.mat';
            fileNameExcel = 'TracksCoordinates.xls';
            pathFileNameMat = [path fileNameMat];
            pathFileNameExcel = [path fileNameExcel];
            pos = f_handles.pos;
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
            f_handles.pos = [];
        end

        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
        set(f_handles.h_FCpb07, 'BackgroundColor', 'red');  
        if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    end
catch ME1
    Message = {'Sorry, it was not possible to delete some tracks.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_removeTrack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_removeTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'rempath.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_removeTrack.
function pb_removeTrack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_removeTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Multiple manually delete cell
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else     
        Message = {'Close this message window, and then click all the tracks you want to delete, then press "Enter".', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        startIm = floor(get(f_handles.h_slider1, 'Value'));
        % Multiple selection with cross shown
        inpImage = imread(f_handles.PathImgCurrent, startIm);
        maxValue = max(inpImage(:));
        [rowI, colI, chI] = size(inpImage);
        x = []; y = [];
        xx = -1; yy = -1;
        while ~isempty(xx) && ~isempty(yy)
            [xx, yy] = ginputc(1, 'Color', 'r');
            if ~isempty(xx) && ~isempty(yy)
                if xx>6 && xx<colI-6 && yy>6 && yy<rowI-6
                    xx = round(xx);
                    yy = round(yy);
                    x = [x; xx];
                    y = [y; yy];
                    inpImage(yy-6:yy+6, xx-2:xx+2) = maxValue-1;
                    inpImage(yy-2:yy+2, xx-6:xx+6) = maxValue-1;
                    name = 'CurrentImageWithCross';
                    newImageName = [pwd filesep 'tmpct' filesep name '.tif'];
                    imwrite(inpImage, newImageName);
                    updateSliceWindows(startIm, newImageName, handles);
                    clear newImageName name
                end
            end
        end
        clear inpImage
        updateSliceWindows(startIm, f_handles.PathImgCurrent, handles);
        numCells = length(x);
        
        h_wait = waitbar(0, 'Please wait...');
        for k = 1:numCells
            idx = f_handles.cellIndex(y(k), x(k));
            if idx>0            
                posizioni = f_handles.pos;
                listIdx = find(posizioni(:, 4) == idx);
                posizioni(listIdx, :) = [];
                f_handles.pos = posizioni;
                
                % Save output
                path = [pwd filesep 'tmpct' filesep];
                fileNameMat = 'TracksCoordinates.mat';
                fileNameExcel = 'TracksCoordinates.xls';
                pathFileNameMat = [path fileNameMat];
                pathFileNameExcel = [path fileNameExcel];
                if ~isempty(f_handles.pos)
                    pos = f_handles.pos;
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
                else
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
                end
                
                clear listIdx posizioni
            end
            waitbar(k/numCells)
        end
        
        % If no tracks close statistics if was opened
        if isempty(f_handles.pos)
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
        end
        
        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
        
        if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;

    end
catch ME1
    if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
    Message = {'Sorry, it was not possible to delete some tracks.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_removePart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_removePart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'rempath_fromhere.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_removePart.
function pb_removePart_Callback(hObject, eventdata, handles)
% hObject    handle to pb_removePart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Multiple delete cell track from the current time point
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        Message = {'Move the slider to the first image you want use as starting point to delete the tracks.', ...
            ' ', ...   
            'Close this message window, and then click all the tracks you want to delete, then press "Enter".', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        startIm = floor(get(f_handles.h_slider1, 'Value'));
        % Multiple selection with cross shown
        inpImage = imread(f_handles.PathImgCurrent, startIm);
        maxValue = max(inpImage(:));
        [rowI, colI, chI] = size(inpImage);
        x = []; y = [];
        xx = -1; yy = -1;
        while ~isempty(xx) && ~isempty(yy)
            [xx, yy] = ginputc(1, 'Color', 'r');
            if ~isempty(xx) && ~isempty(yy)
                if xx>6 && xx<colI-6 && yy>6 && yy<rowI-6
                    xx = round(xx);
                    yy = round(yy);
                    x = [x; xx];
                    y = [y; yy];
                    inpImage(yy-6:yy+6, xx-2:xx+2) = maxValue-1;
                    inpImage(yy-2:yy+2, xx-6:xx+6) = maxValue-1;
                    name = 'CurrentImageWithCross';
                    newImageName = [pwd filesep 'tmpct' filesep name '.tif'];
                    imwrite(inpImage, newImageName);
                    updateSliceWindows(startIm, newImageName, handles);
                    clear newImageName name
                end
            end
        end
        clear inpImage
        updateSliceWindows(startIm, f_handles.PathImgCurrent, handles);
        numCells = length(x);
        
        v = floor(get(f_handles.h_slider1, 'Value'));
        
        h_wait = waitbar(0, 'Please wait...');
        for k = 1:numCells
            idx = f_handles.cellIndex(y(k), x(k));
            if idx>0
                posizioni = f_handles.pos;
                listIdx = find(posizioni(:, 4) == idx);
                listIdx2 = find(posizioni(listIdx, 3) >= v);
                posizioni(listIdx(listIdx2), :) = [];
                f_handles.pos = posizioni;
                
                % Save output
                path = [pwd filesep 'tmpct' filesep];
                fileNameMat = 'TracksCoordinates.mat';
                fileNameExcel = 'TracksCoordinates.xls';
                pathFileNameMat = [path fileNameMat];
                pathFileNameExcel = [path fileNameExcel];
                if ~isempty(f_handles.pos)
                    pos = f_handles.pos;
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
                else
                    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
                    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
                end
                
                clear listIdx listIdx2 posizioni
            end
            waitbar(k/numCells)
        end
        
        % If no tracks close statistics if was opened
        if isempty(f_handles.pos)
            if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
        end

        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
        
        if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
    end
catch ME1
    if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
    Message = {'Sorry, it was not possible to delete some tracks.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_merge1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_merge1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'merge_FP.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_merge1.
function pb_merge1_Callback(hObject, eventdata, handles)
% hObject    handle to pb_merge1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Merge tracks button 1
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        Message = {'Move the slider to the first image you want use as starting point to merge the tracks.', ...
            ' ', ...   
            'Close this message window, and then select the two tracks you want to merge.', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        [x, y] = ginputc(2, 'Color', 'r');
        x = round(x);
        y = round(y);
        numCells = length(x);
        
        track1idx = double(f_handles.cellIndex(y(1), x(1)));
        track2idx = double(f_handles.cellIndex(y(2), x(2)));
        if track1idx==track2idx
            % If two clicks overlap this is to find the nearest track
            CellIndexes = f_handles.cellIndex;
            Mask = zeros(size(CellIndexes));
            Mask(CellIndexes==track1idx) = 1;
            se = strel('square', 3);
            MaskDilDif = imdilate(Mask, se) - Mask;
            inds = find(MaskDilDif==1);
            trackInds = unique(CellIndexes(inds));
            if ~isempty(trackInds) && trackInds(end)~=0
                track2idx = trackInds(end);
            end
        end
        if track1idx==0 || track2idx==0
            Message = {'You must click two existing tracks.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        posizioni = double(f_handles.pos);
        
        % Merge tracks computing average
        list1idx = find(posizioni(:, 4) == track1idx);
        list2idx = find(posizioni(:, 4) == track2idx);
        posizioni1idx = posizioni(list1idx, :);
        posizioni2idx = posizioni(list2idx, :);
        firstIndex = min([posizioni1idx(:, 3); posizioni2idx(:, 3)]);
        secondIndex = max([posizioni1idx(:, 3); posizioni2idx(:, 3)]);      
        posizioni([list1idx; list2idx], :) = [];
        for k = firstIndex:1:secondIndex
            idx1 = find(posizioni1idx(:,3)==k);
            idx2 = find(posizioni2idx(:,3)==k);
            if ~isempty(idx1) && ~isempty(idx2)
                posizioniAverageX = (posizioni1idx(idx1,1) + posizioni2idx(idx2,1))/2;
                posizioniAverageY = (posizioni1idx(idx1,2) + posizioni2idx(idx2,2))/2;
            elseif ~isempty(idx1) && isempty(idx2)
                posizioniAverageX = posizioni1idx(idx1,1);
                posizioniAverageY = posizioni1idx(idx1,2);
            elseif isempty(idx1) && ~isempty(idx2)
                posizioniAverageX = posizioni2idx(idx2,1);
                posizioniAverageY = posizioni2idx(idx2,2);
            end
            if ~isempty(idx1) || ~isempty(idx2)
                posizioniAverageX = round(posizioniAverageX);
                posizioniAverageY = round(posizioniAverageY);
                posizioniNewLine = [posizioniAverageX, posizioniAverageY, k, track1idx];
                posizioni = [posizioni; posizioniNewLine];
            end
            clear posizioniNewLine posizioniAverageY posizioniAverageX idx2 idx1
        end
        posizioni = sortrows(posizioni, [4 3]);
        f_handles.pos = posizioni;
        
        % Save output
        path = [pwd filesep 'tmpct' filesep];
        fileNameMat = 'TracksCoordinates.mat';
        fileNameExcel = 'TracksCoordinates.xls';
        pathFileNameMat = [path fileNameMat];
        pathFileNameExcel = [path fileNameExcel];
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
        else
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
        end
 
        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
    end    
catch ME1
    Message = {'Sorry, it was not possible to merge the two tracks.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_exportTrack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_exportTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'file_save_track.png']);
% RGB_bg_image = ind2rgb(bg_image,map);
% set(handles.output, 'CData', RGB_bg_image);
set(handles.output, 'CData', bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_exportTrack.
function pb_exportTrack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_exportTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f_handles

%% Export tracks as Matlab file
try
    colore = get(f_handles.h_FCpb02, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
            [file, path] = uiputfile('TracksCoordinates.mat','Save file');
            if isequal(file,0) || isequal(path,0)
                Message = {'Saving process failed.', ...
                    ' '};
                msgbox(Message,'Message')
            else
                matName = [path file];
                if isequal(exist(matName, 'file'),2); delete(matName); save(matName, 'pos'); else save(matName, 'pos'); end;
            end
        else
            Message = {'No track is available.', ...
                ' '};
            msgbox(Message,'Message')
        end
    end
catch ME1
    Message = {'No track is available.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_importTrack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_importTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'file_open_track.png']);
% RGB_bg_image = ind2rgb(bg_image,map);
% set(handles.output, 'CData', RGB_bg_image);
set(handles.output, 'CData', bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_importTrack.
function pb_importTrack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_importTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f_handles

%% Import tracks as Matlab file
try
    colore = get(f_handles.h_FCpb02, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        [file, path, filterindex] = uigetfile('*.mat', 'Open a file', 'MultiSelect', 'off');
        if isequal(file,0) || isequal(path,0)
%             Message = {'Loading process failed.', ...
%                 ' '};
%             msgbox(Message,'Message')
        else
            matName = [path file];
            load(matName, 'pos');
            maxFrame = max(pos(:,3));
            maxRowY = max(pos(:,1));
            maxColX = max(pos(:,2));
            inpImage = imread(f_handles.PathImgCurrent, 1);
            [rowI, colI, chI] = size(inpImage);
            numImages =  length(imfinfo(f_handles.PathImgCurrent));
            if maxRowY>rowI || maxColX>colI || maxFrame>numImages
                Message = {'The tracks can not be applied to the image opened.', ...
                    ' '};
                msgbox(Message,'Message')
            else
                f_handles.pos = [];
                f_handles.pos = pos;
                guidata(hObject, handles);
                updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
                set(f_handles.h_FCpb07, 'BackgroundColor', 'yellow');
            end
        end
    end
catch ME1
    Message = {'No track is available.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_movePoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_movePoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'movePoint.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_movePoint.
function pb_movePoint_Callback(hObject, eventdata, handles)
% hObject    handle to pb_movePoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Move track point
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        Message = {'Move the slider to the image (timepoint) where you want move the track.', ...
            ' ', ...   
            'Close this message window, and then select the track you want to move and the new position.', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        [x, y] = ginputc(2, 'Color', 'g');
        x = round(x);
        y = round(y);
        
        track1idx = double(f_handles.cellIndex(y(1), x(1)));
        track2idx = double(f_handles.cellIndex(y(2), x(2)));
        
        % Check error click track
        trackIdx = track1idx;
        NewPositionYrow = y(2);
        NewPositionXcol = x(2);
        if track1idx==0
            trackIdx = track2idx;
            NewPositionYrow = y(1);
            NewPositionXcol = x(1);
            if track2idx==0
                Message = {'You must click an existing track.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
        end
        
        % Check error click new position
        inpImage = imread(f_handles.PathImgCurrent, 1);
        [rowI, colI, chI] = size(inpImage);
        if NewPositionYrow<1 || NewPositionXcol<1 || NewPositionYrow>rowI || NewPositionXcol>colI
            Message = {'You must click on a possible new position in the image.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        
        % Move track in the current image
        v = floor(get(f_handles.h_slider1, 'Value'));
        posizioni = double(f_handles.pos);
        listIdx = find(posizioni(:, 4) == trackIdx);
        posizioniIdx = posizioni(listIdx, :);
        currentIdx = find(posizioniIdx(:,3) == v);
        posizioni(listIdx(currentIdx),1) = NewPositionYrow;
        posizioni(listIdx(currentIdx),2) = NewPositionXcol;
        f_handles.pos = posizioni;
        clear posizioni
        
        % Save output
        path = [pwd filesep 'tmpct' filesep];
        fileNameMat = 'TracksCoordinates.mat';
        fileNameExcel = 'TracksCoordinates.xls';
        pathFileNameMat = [path fileNameMat];
        pathFileNameExcel = [path fileNameExcel];
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
        else
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
        end
 
        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
    end    
catch ME1
    Message = {'Sorry, it was not possible to move the track.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_moveTrack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_moveTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'moveTrack.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_moveTrack.
function pb_moveTrack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_moveTrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Move track point
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        Message = {'Move the slider to the image (timepoint) from where you want move the track.', ...
            ' ', ...   
            'Close this message window, and then select the track you want to move and the shift.', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        [x, y] = ginputc(2, 'Color', 'r');
        
        x = round(x);
        y = round(y);
        
        track1idx = double(f_handles.cellIndex(y(1), x(1)));
        track2idx = double(f_handles.cellIndex(y(2), x(2)));
        
        % Check error click track
        trackIdx = track1idx;
        OldPositionYrow = y(1);
        OldPositionXcol = x(1);
        NewPositionYrow = y(2);
        NewPositionXcol = x(2);
        if track1idx==0
            trackIdx = track2idx;
            OldPositionYrow = y(2);
            OldPositionXcol = x(2);
            NewPositionYrow = y(1);
            NewPositionXcol = x(1);
            if track2idx==0
                Message = {'You must click an existing track.', ...
                    ' '};
                msgbox(Message,'Message')
                return
            end
        end
        
        % Check error click new position
        inpImage = imread(f_handles.PathImgCurrent, 1);
        [rowI, colI, chI] = size(inpImage);
        if NewPositionYrow<1 || NewPositionXcol<1 || NewPositionYrow>rowI || NewPositionXcol>colI
            Message = {'You must click on a possible new position in the image.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        
        % Shift
        ShiftYrow = NewPositionYrow-OldPositionYrow;
        ShiftXcol = NewPositionXcol-OldPositionXcol;
        
        % Move track from the current image
        v = floor(get(f_handles.h_slider1, 'Value'));
        posizioni = double(f_handles.pos);
        listIdx = find(posizioni(:, 4) == trackIdx);
        posizioniIdx = posizioni(listIdx, :);
        currentIdx = find(posizioniIdx(:,3) >= v);
        if ~isempty(currentIdx)
            for p = 1:length(currentIdx)
                NewTrackPosYrow = posizioni(listIdx(currentIdx(p)),1) + ShiftYrow;
                if NewTrackPosYrow<1; NewTrackPosYrow=1; end
                if NewTrackPosYrow>rowI; NewTrackPosYrow=rowI; end
                NewTrackPosXcol = posizioni(listIdx(currentIdx(p)),2) + ShiftXcol;
                if NewTrackPosXcol<1; NewTrackPosXcol=1; end
                if NewTrackPosXcol>colI; NewTrackPosYXcol=colI; end
                posizioni(listIdx(currentIdx(p)),1) = NewTrackPosYrow;
                posizioni(listIdx(currentIdx(p)),2) = NewTrackPosXcol;
            end
        end
        f_handles.pos = posizioni;
        clear posizioni
        
        % Save output
        path = [pwd filesep 'tmpct' filesep];
        fileNameMat = 'TracksCoordinates.mat';
        fileNameExcel = 'TracksCoordinates.xls';
        pathFileNameMat = [path fileNameMat];
        pathFileNameExcel = [path fileNameExcel];
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
        else
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
        end
 
        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
    end    
catch ME1
    Message = {'Sorry, it was not possible to move the track.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_changeID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_changeID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'id.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_changeID.
function pb_changeID_Callback(hObject, eventdata, handles)
% hObject    handle to pb_changeID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Change ID of a track
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        
        Message = {'Move the slider to the image (timepoint) where there is a track of whose ID you want to change.', ...
            ' ', ...   
            'Close this message window, and then select the track whose ID you want to change.', ...  
            ' '};
        h_msg = msgbox(Message,'Message');
        waitfor(h_msg);
        axes(f_handles.h_axes1);
        
        [x, y] = ginputc(1, 'Color', 'r');
        x = round(x);
        y = round(y);
        
        track1idx = double(f_handles.cellIndex(y(1), x(1)));
        
        % Check error click track
        if track1idx==0
            Message = {'You must click an existing track.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end

        % dialog
        prompt = {'Old ID:','New ID:','Show IDs (1 means enable)'};
        dlg_title = 'ID change';
        num_lines = 1;
        def = {num2str(track1idx), num2str(track1idx), num2str(f_handles.showImageID)};
        answer = inputdlg(prompt,dlg_title,num_lines,def,'on');
        
        if isempty(answer)
            return
        end
        OldID = track1idx;
                    
        % Errors check
        flag_ComputationOn = 1;
        if ~isempty(answer)
            NewID = str2num(answer{2});
            if NewID<1
                flag_ComputationOn = 0;
            end
            flagShowIDs = str2num(answer{3});
        else
            flag_ComputationOn = 0;
        end
        if flag_ComputationOn==0
            Message = {'Wrong input parameter.', ...
                ' '};
            msgbox(Message,'Message')
            return
        end
        
        try
            flagShowImageID = str2num(answer{3});
            if ~isempty(flagShowImageID)
                f_handles.showImageID = flagShowImageID;
            end
        catch ME1
            f_handles.showImageID = 1;
        end
        
        cellIndex = double(f_handles.cellIndex);
        pos = f_handles.pos;
        cellIDs = unique(pos(:,4));
        indVect = find(cellIDs==NewID);
        if isempty(indVect)
            indToBeChanged = find(pos(:,4)==OldID);
            pos(indToBeChanged,4) = NewID;
            %indToBeChanged2D = find(cellIndex==OldID);
            %cellIndex(indToBeChanged2D) = NewID;
        else
            indAlreadyPresent = find(pos(:,4)==NewID);
            indToBeChanged = find(pos(:,4)==OldID);
            pos(indToBeChanged,4) = NewID;
            pos(indAlreadyPresent,4) = OldID;
            %indAlreadyPresent2D = find(cellIndex==NewID);
            %indToBeChanged2D = find(cellIndex==OldID);
            %cellIndex(indToBeChanged2D) = NewID;
            %cellIndex(indAlreadyPresent2D,4) = OldID;
        end
        f_handles.pos = pos;
        f_handles.cellIndex = double(cellIndex);
        
        v = floor(get(f_handles.h_slider1, 'Value'));
        guidata(hObject, handles);
        updateSliceWindows(v, f_handles.PathImgCurrent, handles);
      
        % Show figure with currect IDs
        if f_handles.showImageID == 1
            inputImage = imread(f_handles.PathImgCurrent, 1);
            [row, col, ch] = size(inputImage);
            if ~isa(inputImage, 'uint8')
                inputImage = double(inputImage);
                inputImage = (inputImage - min(min(inputImage)));
                inputImage = inputImage ./ max(max(inputImage));
                inputImage = inputImage .* 256;
                inputImage = uint8(inputImage);
            end
            out(:,:,1) = inputImage;
            out(:,:,2) = inputImage;
            out(:,:,3) = inputImage;

            pos = f_handles.pos;
            cellID = unique(pos(:,4));
            numCell = length(cellID);

            figure, imshow(out, [], 'Border', 'Tight'), title('Track IDs');
            hold on
            for i = 1:numCell
                indecesI = find(pos(:,4)==cellID(i));
                firstYrow = pos(indecesI(1),1);
                firstXcol = pos(indecesI(1),2);
                colorr = mod(cellID(i) * 1237, 255)/255;
                colorg = mod(cellID(i) * 59, 255)/255;
                colorb = mod(cellID(i) * 37, 255)/255;
                text(firstXcol, firstYrow, num2str(cellID(i)), 'Color', [colorr, colorg, colorb], 'EdgeColor', 'white', 'HorizontalAlignment','center');
            end
        end
    end    
catch ME1
    Message = {'Sorry, an error occurred.', ...
        ' '};
    msgbox(Message,'Message')
end


%% FLOW CHART: "STATISTICS" FUNCTIONS


% --- Executes on button press in FCpb07.
function FCpb07_Callback(hObject, eventdata, handles)
% hObject    handle to FCpb07 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Statistics
global f_handles

try
    if isempty(f_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        clear pos
        pos = f_handles.pos;
        pos = sortrows(pos, [4 3]);
        f_handles.h_GUIstatistics = CellTrackerStatistics(pos);
    end
catch ME1
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    Message = {'Statistics computation fails.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_3Dplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_3Dplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'tracks.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


function pb_3Dplot_Callback(hObject, eventdata, handles)
% hObject    handle to pb_3Dplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% plot tracks in 3d
global f_handles

try
    if isfield(f_handles, 'pos') && ~isempty(f_handles.pos)
        
        pos = f_handles.pos;
        maxind = max(pos(:, 4));
        minFrame = min(pos(:, 3));
        maxFrame = max(pos(:, 3));
        inpImage = imread(f_handles.PathImgOriginal, minFrame);
        
        % Conversion for visualization
        inpImage = double(inpImage);
        inpImage = 255*(inpImage - min(inpImage(:)))/max(inpImage(:) - min(inpImage(:)));
        inpImage = round(inpImage);
        inpImage = uint8(inpImage);
        
        [rowI, colI, chI] = size(inpImage);
        figure('Name','3D plot of tracks');
        hold on
        surf(minFrame*ones(size(inpImage)),inpImage,'FaceColor','texturemap','EdgeColor','none');
        colormap gray;
        set(gca,'xlim',[1 colI],'ylim',[1 rowI],'zlim',[minFrame maxFrame+1])
        view(-35,45)
        for i=1:maxind
            idxlist = find(pos(:, 4) == i);
            if ~isempty(idxlist)
                y = pos(idxlist, 1);
                x = pos(idxlist, 2);
                z = pos(idxlist, 3);
                colorr = mod(i * 1237, 255);
                colorg = mod(i * 59  , 255);
                colorb = mod(i * 37  , 255);
                color = double([colorr colorg colorb]) / 255;
                plot3(x, y, z, 'Color', color);
                statusText = sprintf('[%d]', i);
                %text(x(1) , y(1) , z(1)-1, statusText, 'Color', 'black', 'HorizontalAlignment','center');
                text(x(length(x)) , y(length(x)) , z(length(x))+1, statusText, 'Color', 'black',  'HorizontalAlignment','center');
                set(findobj('Type','line'),'LineWidth', 2);
                hold on;
            end
        end
        hold off
    else
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    end
catch ME1
    Message = {'No track available.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_video_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% --- Executes on button press in pb_3Dplot.
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'video.gif']);
RGB_bg_image = ind2rgb(bg_image,map);
set(handles.output, 'CData', RGB_bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_video.
function pb_video_Callback(hObject, eventdata, handles)
% hObject    handle to pb_video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Save tracked data to an out.tif file
global f_handles

try
    colore = get(f_handles.h_FCpb02, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        if ~isempty(f_handles.PathImgCurrent) && ~isempty(f_handles.pos)
            [file, path] = uiputfile('MovieTrack.avi','Save file');
            if isequal(file,0) || isequal(path,0)
                Message = {'Saving process failed.', ...
                    ' '};
                msgbox(Message,'Message')
            else
                fileNameVideo = [path file];
                numImages =  length(imfinfo(f_handles.PathImgOriginal));
                mov = VideoWriter(fileNameVideo);
                open(mov);
                for i = 1:numImages
                    guidata(hObject, handles);
                    updateSliceWindows(i, f_handles.PathImgCurrent, handles);
                    F = getframe(f_handles.h_axes1);
                    writeVideo(mov, F);
                end
                close(mov);
                guidata(hObject, handles);
                updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
            end
        else
            Message = {'No track available.', ...
                ' '};
            msgbox(Message,'Message')
        end
    end
catch ME1
    Message = {'Sorry, it was not possible to create a video.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function pb_savestack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pb_savestack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.output = hObject;
[bg_image,map]=imread(['icons' filesep 'file_save_images.png']);
% RGB_bg_image = ind2rgb(bg_image,map);
% set(handles.output, 'CData', RGB_bg_image);
set(handles.output, 'CData', bg_image);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_savestack.
function pb_savestack_Callback(hObject, eventdata, handles)
% hObject    handle to pb_savestack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f_handles

%% Save the current image stack
try
    colore = get(f_handles.h_FCpb02, 'BackgroundColor');
    if isequal(colore,[1,0,0]);
        Message = {'No image is open.', ...
            ' '};
        msgbox(Message,'Message')
    else
        if ~isempty(f_handles.PathImgCurrent)
            [file, path] = uiputfile('CurrentImageStack.tif','Save file');
            if isequal(file,0) || isequal(path,0)
                Message = {'Saving process failed.', ...
                    ' '};
                msgbox(Message,'Message')
            else
                filename = [path file];
                numImages =  length(imfinfo(f_handles.PathImgOriginal));
                h_wait = waitbar(0, 'Please wait...');
                for i = 1:numImages
                    imagei= imread(f_handles.PathImgCurrent, i); 
                    if i==1
                        imwrite(imagei, filename);
                    else
                        imwrite(imagei, filename, 'WriteMode', 'append');
                    end
                    waitbar(i/numImages)
                end
                if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;
            end
        else
            Message = {'No image is open.', ...
                ' '};
            msgbox(Message,'Message')
        end
    end
catch ME1
    Message = {'No image is open.', ...
        ' '};
    msgbox(Message,'Message')
end


%% HELP BUTTONS


% --- Executes on button press in help01.
function help01_Callback(hObject, eventdata, handles)
% hObject    handle to help01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Load an image file": please load an image file (e.g., TIFF) containing at least two frames in order to perform cell tracking. All Bioformat files (www.bioformat.org) are accepted.', ...
    ' ', ...
    '"Crop time": this option allows the user to remove unwanted frames from the previously loaded input image file. Select the first and the last frame of interest.', ...
        ' ', ...
    '"Crop x-y": crops the frames of the loaded input image file by drawing a rectangle around the region of interest to be kept.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help02.
function help02_Callback(hObject, eventdata, handles)
% hObject    handle to help02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Vignetting correction": corrects the uneven intensity distribution in the loaded images. Different algorithms are available (see "Settings").', ...
    ' ', ...
    '"Settings": allows modification of the default internal parameters and the method used to perform the vignetting correction.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help03.
function help03_Callback(hObject, eventdata, handles)
% hObject    handle to help03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Automatic alignment": corrects global x-y paraxial shift between the loaded images (see "Settings").', ...
    ' ', ...
    '"Settings": allows modification of the default internal parameters used to perform the automatic alignment.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help04.
function help04_Callback(hObject, eventdata, handles)
% hObject    handle to help04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Automatic tracking": setting window opens automatically when the button is pressed. Draw a rectangle around a representative cell. Use "Edit tracks" for post-processing the tracks.', ...
    ' ', ...
    '"Cell detector threshold": template matches higher than THRESHOLD level will be seletced as objects of interest. Briefly: the AVERAGE intensity across the image is computed and the cells to be tracked are defined as the objects having higher intensity than "THRESHOLD x AVERAGE". If THRESHOLD too high, the cells will not be detected. If too low, many debris will be considered as cells.', ...
    ' ', ...
    '"Memory (for cells lost)": number of frames still considered when a tracked cell becomes temporarily undetected (e.g., it goes out of focus or out of the field of view).', ...
    ' ', ...
    '"Minimal track length": minimum number of frames for which a cell must be tracked in order to be included in the analysis.', ...
    ' ', ...
    '"Maximal cell displacement": maximal expected displacement (distance measured in pixels) between consecutive frames.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help05.
function help05_Callback(hObject, eventdata, handles)
% hObject    handle to help05 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Semi-automatic tracking": select the initial frame and the last frame to be considered for tracking. Alternatively, you can move the slider to the first frame to be used as a starting point to track the cells. Click all the cells you want to track, and then press "Enter". Use "Edit tracks" for post-processing the tracks.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help06.
function help06_Callback(hObject, eventdata, handles)
% hObject    handle to help06 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Manual tracking": select the initial frame, the last frame and the step size (i.e., the number of images skipped, so only every # frames must be clicked). Click the centroid of the cell you want to track. The slider and the displayed image will be updated upon clicking. If you want to terminate the process before the last frame, simply press "Enter". Use "Edit tracks" for post-processing the tracks.', ...
    ' '};
msgbox(Message,'Help message')


% --- Executes on button press in help07.
function help07_Callback(hObject, eventdata, handles)
% hObject    handle to help07 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'DESCRIPTION:', ...
    ' ', ...
    '"Statistics": automatically computes several statistics about individual tracks in every time point, tracked cells (for each cell throughout the computed time points), and the whole culture (i.e., all the cells considered together).', ...
    ' ', ...
    '"3D tracks plot": displays the x-y positions of the tracked cell centroids over time. The z-axis represents the number of frames (i.e., time points).', ...
        ' ', ...
    '"Create a video": creates a video (.avi format) which shows the tracked cells outlined by colored squares.', ...
        ' ', ...
    '"Save the current stack of images": saves the currently shown stack as a TIFF file.', ...
    ' '};
msgbox(Message,'Help message')


%% BUTTON SETTINGS


% --- Executes on button press in pb_settings01.
function pb_settings01_Callback(hObject, eventdata, handles)
% hObject    handle to pb_settings01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles

% call the settings window
colore = get(f_handles.h_FCpb01, 'BackgroundColor');
if isequal(colore,[0,1,0]); 
    f_handles.h_GUIsetG = CellTrackerSettingsGeneral;
else
    Message = {'No image is open.', ...
        ' '};
    msgbox(Message,'Message')
end
    

% --- Executes on button press in pb_settings02.
function pb_settings02_Callback(hObject, eventdata, handles)
% hObject    handle to pb_settings02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles

% call the settings window
colore = get(f_handles.h_FCpb01, 'BackgroundColor');
if isequal(colore,[0,1,0]); 
    f_handles.h_GUIsetG = CellTrackerSettingsGeneral;
else
    Message = {'No image is open.', ...
        ' '};
    msgbox(Message,'Message')
end
