function [VectorAngle, VectorModule, Vector_xcol_yrow] = relativeVectorModuleAngle(Tail_xcol_yrow, Head_xcol_yrow)
% AUTHOR: Filippo Piccinini (E-mail: f.piccinini@unibo.it)
% DATE: 27 May 2015
% NAME: relativeVectorModuleAngle
% 
% Given two input 2D vectors defined by their [x, y] = [col, row]
% coordinates, this function computes the angle, the module and the new [x,
% y] coordinates of the vector obtained performing the "vector product"
% operation (called also: cross product).
%
% PARAMETERS:
% 	Tail_xcol_yrow      Input vector coordinates (in pixels) defined as: 
%                       [Vector1xcol, Vector1yrow];
% 	Head_xcol_yrow      Input vector coordinates (in pixels) defined as:
%                       [Vector2xcol, Vector2yrow];
%
% OUTPUT:
%   VectorAngle         Degrees of direction of the output vector.
%   VectorModule        Length (in pixels) of the output vector.
% 	Vector_xrow_ycol	Output vector head coordinates (in pixels) defined
%                       as: [VectorXcol, VectorYrow];
%
% EXAMPLE OF USAGE: 
% [VectorAngle, VectorModule, Vector_xrow_ycol] = relativeVectorModuleAngle([2,0], [0,3]);
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

Tailx = Tail_xcol_yrow(1);
Taily = Tail_xcol_yrow(2);

Headx = Head_xcol_yrow(1);
Heady = Head_xcol_yrow(2);

VectorX = Headx - Tailx;
VectorY = Heady - Taily;
Vector_xcol_yrow = [VectorX, VectorY];
VectorModule = sqrt(VectorX.^2 + VectorY.^2);

VectorAngleCos = acosd(VectorX/VectorModule);
if sign(VectorY)>=0
    VectorAngle = 360-VectorAngleCos;
else
    VectorAngle = VectorAngleCos;
end