function showTableData(Table, ColumnLabels, WindowsName)
% AUTHOR: Filippo Piccinini (E-mail: f.piccinini@unibo.it)
% DATE: May 27, 2015
% NAME: showTableData (version 1.0)
% 
% To show the data of a table.
%
% PARAMETERS:
% 	Table               Table that must be shown.
%   ColumnLabels        Array of strings with the names to be visualized 
%                       in the columns
%   WindowsName         Name to be visualized in the figure
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

numCol = length(ColumnLabels);
[row, col, ch] = size(Table);
if numCol~=col
    Message = {'The numebr of columns of the table is not equal to the number of labels.', ...
        ' '};
    msgbox(Message,'Message')
    return
end

cnames = ColumnLabels;
Mdati  = Table;

%__INIZIALIZZAZIONE DEGLI OGGETTI GRAFICI____


%_________FIGURA PRINCIPALE_______________
form1= figure('Tag', 'figura1', ...
    'Position',[400,300,650,400],'Name',WindowsName,...
    'NumberTitle','off','Color',[0.941 0.941 0.941],...
    'ResizeFcn', @StatisticsResizeFcn, ...
    'Resize','on', ...
    'Visible','off');

set(form1, 'MenuBar', 'none'); %To delete: File, Edit, View,
%set(form1, 'ToolBar', 'none');
set(form1, 'Visible','on');

left_bottom_width_height = get(form1, 'Position');
f1_left = left_bottom_width_height(1);
f1_bottom = left_bottom_width_height(2);
f1_width = left_bottom_width_height(3);
f1_height = left_bottom_width_height(4);


% % Testo dentro alla figura nella prima linea
% %_____________STATIC TEXT____________________
% testo = uicontrol(form1,'Style','text','Position',[250,370,100,25],...
%     'String',WindowsName);

%______________TABLE________________
table = uitable('Data',Mdati,'ColumnName',cnames,... 
    'Parent',form1,'Position',[10 50 f1_width-20 f1_height-80]);


%_______________PULSANTI_____________________
Esporta_Excel= uicontrol(form1,'Style','pushbutton',...
    'Position',[20,10,100,25],'String','Save as Excel file',...
    'Callback',@esporta_excel_plot);

Esporta_Matlab= uicontrol(form1,'Style','pushbutton',...
    'Position',[130,10,100,25],...
    'String','Save as Matlab file','Callback',@esporta_matlab_plot);

% Esporta_Txt= uicontrol(form1,'Style','pushbutton',...
%     'Position',[240,10,100,25],...
%     'String','Save as txt file','Callback',@esporta_txt_plot);

Esporta_Csv= uicontrol(form1,'Style','pushbutton',...
    'Position',[240,10,100,25],...
    'String','Save as csv file','Callback',@esporta_csv_plot);

% 
% chiudi = uicontrol(form1,'Style','pushbutton','Position',[200,10,70,25],...
%     'String','Close','Callback',@chiudi_plot);


%________FUNZIONE PULSANTE ESPORTA EXCEL________ 
    function esporta_excel_plot(hObject,eventdata)
        [file,path] = uiputfile('OutputData.xls','Save file');
        if file==0
            return
        end
        pathfile = [path file];
        
        [ro, co] = size(Mdati);
        for in = 1:co
            TableFinal{1,in} = cnames{in};
        end
        if iscell(Mdati)
            for in = 1:co
                for jn = 1:ro            
                    TableFinal{jn+1,in} = Mdati{jn,in};
                end
            end
        elseif ismatrix(Mdati)
            for in = 1:co
                for jn = 1:ro            
                    TableFinal{jn+1,in} = Mdati(jn,in);
                end
            end
        end
        if ismac || isunix
            TableWithoutHeaders = TableFinal(2:end,:);
            xlswrite(pathfile, TableWithoutHeaders);
        else
            xlswrite(pathfile, TableFinal);
        end
    end


%________FUNZIONE PULSANTE ESPORTA MATLAB________ 
    function esporta_matlab_plot(hObject,eventdata)
        [file,path] = uiputfile('OutputData.mat','Save file');
        if file==0
            return
        end
        pathfile = [path file];
        
        [ro, co] = size(Mdati);
        for in = 1:co
            ColumnNames{1,in} = cnames{in};
        end
        
        save(pathfile, 'Mdati', 'ColumnNames');
    end


%__________FUNZIONE PULSANTE ESPORTA TXT__________
    function esporta_txt_plot(hObject,eventdata)
        [file,path] = uiputfile('OutputData.txt','Save file name');
        if file==0
            return
        end
        pathfile = [path file];
        dlmwrite(pathfile, Mdati);
    end


%__________FUNZIONE PULSANTE ESPORTA CSV__________
    function esporta_csv_plot(hObject,eventdata)
        [file,path] = uiputfile('OutputData.txt','Save file name');
        if file==0
            return
        end
        pathfile = [path file];
        csvwrite(pathfile, Mdati)
    end


%__________FUNZIONE PULSANTE CHIUDI_____________
    function chiudi_plot(hObject,eventdata)
        close
    end



%__________StatisticsResizeFcn_____________
    function StatisticsResizeFcn(src,evt)

        try
            left_bottom_width_height = get(src, 'Position');
            f1_left = left_bottom_width_height(1);
            f1_bottom = left_bottom_width_height(2);
            f1_width = left_bottom_width_height(3);
            f1_height = left_bottom_width_height(4);

            clf

            table = uitable('Data',Mdati,'ColumnName',cnames,... 
                'Parent',form1,'Position',[10 50 f1_width-20 f1_height-80]);

            Esporta_Excel= uicontrol(form1,'Style','pushbutton',...
                'Position',[20,10,100,25],'String','Save as Excel file',...
                'Callback',@esporta_excel_plot);

            Esporta_Matlab= uicontrol(form1,'Style','pushbutton',...
                'Position',[130,10,100,25],...
                'String','Save as Matlab file','Callback',@esporta_matlab_plot);

            Esporta_Csv= uicontrol(form1,'Style','pushbutton',...
                'Position',[240,10,100,25],...
                'String','Save as csv file','Callback',@esporta_csv_plot);
        catch ME1
        end
        
    end

end