function Track = trackComputationDynamicApproach(startyrow, startxcol, starttime, lastyrow, lastxcol, lasttime)
% AUTHOR: Filippo Piccinini (E-mail: f.piccinini@unibo.it)
% DATE: June 15, 2015
% NAME: trackComputationDynamicApproach (version 1.0)
% 
% To compute a new track.

% CellTracker Toolbox
% Copyright 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 2 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

global f_handles

waitBarEveryNImages = 6; % The waiting bar is shown only if the step between two clicks is higher than "waitBarEveryNImages".

if isempty(f_handles.CellMaxDisp) || isempty(f_handles.CellTempSize)
    Message = {'Wrong input parameters.', ...
        ' '};
    msgbox(Message,'Message')
    return
else
    dynamicMaxDisp  = f_handles.CellMaxDisp;
    dynamicTempSize = ceil(f_handles.CellTempSize/2); % This to work with radious and not diameter.
end

startImage = imread(f_handles.PathImgCurrent, starttime);
[rowI, colI, chI] = size(startImage);

% Check if the cell is to close to the imge border and in case return empty track
if startxcol-dynamicTempSize<0 || startxcol+dynamicTempSize>colI || startyrow-dynamicTempSize<0 || startyrow+dynamicTempSize>rowI
    Message = {'This cell can be tracked because is to close to the image border.', ...
        ' '};
    msgbox(Message,'Message')
    return
end
if lastxcol-dynamicTempSize<0 || lastxcol+dynamicTempSize>colI || lastyrow-dynamicTempSize<0 || lastyrow+dynamicTempSize>rowI
    Message = {'This cell can be tracked because is to close to the image border.', ...
        ' '};
    msgbox(Message,'Message')
    return
end
    
% Window starttime
ULCstart_yrow = startyrow-dynamicTempSize-dynamicMaxDisp;
if ULCstart_yrow < 1; ULCstart_yrow = 1; end;  
ULCstart_xcol = startxcol-dynamicTempSize-dynamicMaxDisp;
if ULCstart_xcol < 1; ULCstart_xcol = 1; end;
BRCstart_yrow = startyrow+dynamicTempSize+dynamicMaxDisp;
if BRCstart_yrow > rowI; BRCstart_yrow = rowI; end;
BRCstart_xcol = startxcol+dynamicTempSize+dynamicMaxDisp;
if BRCstart_xcol > colI; BRCstart_xcol = colI; end;

% Window lasttime
ULClast_yrow = lastyrow-dynamicTempSize-dynamicMaxDisp;
if ULClast_yrow < 1; ULClast_yrow = 1; end;  
ULClast_xcol = lastxcol-dynamicTempSize-dynamicMaxDisp;
if ULClast_xcol < 1; ULClast_xcol = 1; end;
BRClast_yrow = lastyrow+dynamicTempSize+dynamicMaxDisp;
if BRClast_yrow > rowI; BRClast_yrow = rowI; end;
BRClast_xcol = lastxcol+dynamicTempSize+dynamicMaxDisp;
if BRClast_xcol > colI; BRClast_xcol = colI; end;

% Final window: computed as large window considering the single two windows
ULC_yrow = min([ULCstart_yrow, ULClast_yrow]);
ULC_xcol = min([ULCstart_xcol, ULClast_xcol]);
BRC_yrow = max([BRCstart_yrow, BRClast_yrow]);
BRC_xcol = max([BRCstart_xcol, BRClast_xcol]);

% Save stack images
currentImageName = char(f_handles.PathImgCurrent);
[pathstr, fileName, ext] = fileparts(currentImageName);
outFile = [pwd filesep 'tmpct' filesep fileName '_dynamicTrack.tif'];
if lasttime-starttime > waitBarEveryNImages
    h_wait = waitbar(0, 'Please wait...');
end
for i=starttime:lasttime        
    % read in
    in = imread(currentImageName, i);
    % crop
    in = in(ULC_yrow:BRC_yrow, ULC_xcol:BRC_xcol, :);
    % save
    if i==starttime
        imwrite(in, outFile)
    else
        imwrite(in, outFile, 'WriteMode', 'append');
    end
    if lasttime-starttime > waitBarEveryNImages
        waitbar(i/(lasttime-starttime))
    end
end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;

%% Compute correlation images

% Coordinates of the clicks in the images cut
startyrown = startyrow - ULC_yrow + 1;
startxcoln = startxcol - ULC_xcol + 1;
lastyrown  = lastyrow - ULC_yrow + 1;
lastxcoln  = lastxcol - ULC_xcol + 1;
numFrames = lasttime-starttime+1;

% Compute template in the first image
in = imread(outFile, 1);
[sizeyrow, sizexcol, ch] = size(in);
tempImg = in(startyrown-dynamicTempSize:startyrown+dynamicTempSize, startxcoln-dynamicTempSize:startxcoln+dynamicTempSize);

% Compute correlation images
if numFrames > waitBarEveryNImages
    h_wait = waitbar(0, 'Please wait...');
end
outpos = [];
for i = 1:numFrames %ATTENTION -1 
    searchImg = imread(outFile, i);
    [bigyrow, bigxcol, ch] = size(searchImg);
    
    if f_handles.CellMatchingModality == 1
        NumberOfBins = 20;
        DistanceMetric = 'euclidean';
        step = 1;
        corr = matchHistograms(searchImg, tempImg, NumberOfBins, DistanceMetric, step);
        corr(isnan(corr)) = 0;
        %figure(2); imagesc(corr); drawnow;
    else
        corr = standardCorrelation(searchImg, tempImg, 0);
        %figure(1); imagesc(corr); drawnow;
    end
    
    corrImgOrig{i} = corr;
    clear corr
    
    if numFrames > waitBarEveryNImages
        waitbar(i/(numFrames))
    end
end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;

% Compute resize correlation images
ResizeFactor = 8;
if ResizeFactor==1
    corrImg = corrImgOrig;
else
    for i=1:numel(corrImgOrig)
        se = strel('square',ResizeFactor);
        dilated = imdilate(corrImgOrig{i},se);
        smallimgD = imresize(dilated, 1/ResizeFactor);
        corrImg{i} = smallimgD;
        clear smallimgD dilated
    end
end

% Coordinates of the clicks in the rescaled images
startyrowns = round(startyrown/ResizeFactor);
startxcolns = round(startxcoln/ResizeFactor);
lastyrowns  = round(lastyrown/ResizeFactor);
lastxcolns  = round(lastxcoln/ResizeFactor);
searchRegSize = round(dynamicMaxDisp/ResizeFactor); % ATTENTION

% direciton matrix
movx = [];
movy = [];

% first plane: all must come from the start position
% last plane: we are interested only in the stop positon
[syrow, sxcol] = size(corrImg{1});

% cost matrix
costImage = zeros(syrow, sxcol, numel(corrImg));
costImage(startyrowns, startxcolns, 1) = 1;

% Computation
if numel(corrImg) > waitBarEveryNImages
    h_wait = waitbar(0, 'Please wait...');
end
side = searchRegSize*2+1;
sigma = side;
Gaussian2DWeight = gauss2d(zeros(side), sigma, [ceil(side/2),ceil(side/2)]);
for i = 1:numel(corrImg)

    currentCorrImg = corrImg{i};
    
    for y = searchRegSize+1:1:syrow-searchRegSize       
        for x = searchRegSize+1:1:sxcol-searchRegSize

            % Search region: we should take care of the maximum shift of the cell
            searchReg = costImage(y-searchRegSize:y+searchRegSize, x-searchRegSize:x+searchRegSize, i);
            searchReg = searchReg.*Gaussian2DWeight;
            
            % Find the maximum
            [maxv, maxi] = max(searchReg(:));
            [dyrow, dxcol] = ind2sub(size(searchReg),maxi);
            
            % Update cost function
            costImage(y, x, i+1) = maxv + currentCorrImg(y, x);
            
            % Update motion matrices
            movy(y, x, i+1) = dyrow-searchRegSize-1;
            movx(y, x, i+1) = dxcol-searchRegSize-1;
            
        end
    end
    if numel(corrImg) > waitBarEveryNImages
        waitbar(i/(numel(corrImg)))
    end
end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;


%% Track back the path starting from the stop position
if numel(corrImg) > waitBarEveryNImages
    h_wait = waitbar(0, 'Please wait...');
end
outpos = [];
for i = numel(corrImg):-1:1
    % To track back the cell starting from the stop position

    if i == numel(corrImg)
        currPosX = lastxcolns;
        currPosY = lastyrowns;
    else
        currPosXNew = currPosX + movx(currPosY, currPosX, i);
        currPosYNew = currPosY + movy(currPosY, currPosX, i);        
        currPosX = currPosXNew;
        currPosY = currPosYNew;
    end
    
    if i == numel(corrImg)    
%         % Visualization
%         %img = corrImgOrig{i};
%         img = imread(outFile, i);
%         currPosYfull = lastyrowns.*ResizeFactor;
%         currPosXfull = lastxcolns.*ResizeFactor;   
%         img(currPosYfull-10:currPosYfull+10, currPosXfull) = 0; % This is a cross
%         img(currPosYfull, currPosXfull-10:currPosXfull+10) = 0; % This is a cross        
%         img(lastyrown-10:lastyrown+10, lastxcoln) = max(img(:)); % This is a cross
%         img(lastyrown, lastxcoln-10:lastxcoln+10) = max(img(:)); % This is a cross        
%         figure(1), imshow(img, [], 'Border', 'Tight')
%         pause(.5);
        
        outpos = [outpos;  [lastyrown, lastxcoln, i 1]];
    elseif i == 1
%         % Visualization
%         %img = corrImgOrig{i};
%         img = imread(outFile, i);
%         currPosYfull = startyrowns.*ResizeFactor;
%         currPosXfull = startxcolns.*ResizeFactor;   
%         img(currPosYfull-10:currPosYfull+10, currPosXfull) = 0; % This is a cross
%         img(currPosYfull, currPosXfull-10:currPosXfull+10) = 0; % This is a cross        
%         img(startyrown-10:startyrown+10, startxcoln) = max(img(:)); % This is a cross
%         img(startyrown, startxcoln-10:startxcoln+10) = max(img(:)); % This is a cross        
%         figure(1), imshow(img, [], 'Border', 'Tight')
%         pause(.5);
        
        outpos = [outpos;  [startyrown, startxcoln, i, 1]];
    else
        % Coordinates in the full-resolution frame
        currPosYfull = currPosY.*ResizeFactor;
        currPosXfull = currPosX.*ResizeFactor;
        
        ResizeFactor2 = ResizeFactor*2; %ATTENTION: it should be *1!
                
        % Maximum in the corrImgOrig
        img = corrImgOrig{i};
        searchRegFull = zeros(sizeyrow, sizexcol);
        ystart = currPosYfull-ResizeFactor2+1;
        yend = currPosYfull+ResizeFactor2;
        xstart = currPosXfull-ResizeFactor2+1;
        xend = currPosXfull+ResizeFactor2;
        if ystart<1; ystart = 1; end;
        if xstart<1; xstart = 1; end;
        if yend>sizeyrow; yend = sizeyrow; end;
        if xend>sizexcol; xend = sizexcol; end;
        searchRegFull(ystart:yend, xstart:xend) = img(ystart:yend, xstart:xend);
        [maxv, maxi] = max(searchRegFull(:));
        [fyrow, fxcol] = ind2sub(size(searchRegFull),maxi);
        clear searchRegFull
        
%         % Visualization
%         img = imread(outFile, i);
%         img(currPosYfull-10:currPosYfull+10, currPosXfull) = 0; % This is a cross
%         img(currPosYfull, currPosXfull-10:currPosXfull+10) = 0; % This is a cross 
%         img(fyrow-10:fyrow+10, fxcol) = max(img(:)); % This is a cross
%         img(fyrow, fxcol-10:fxcol+10) = max(img(:)); % This is a cross        
%         figure(1), imshow(img, [], 'Border', 'Tight')
%         pause(.5);
        
        outpos = [outpos;  [fyrow, fxcol, i, 1]];
    end
    if numel(corrImg) > waitBarEveryNImages
        waitbar((i-numel(corrImg))/(numel(corrImg)))
    end
end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;

%% Output
outpos = sortrows(outpos, [4 3]);
Track = NaN*ones(size(outpos));
Track(:,1) = outpos(:,1)+ULC_yrow-1;
Track(:,2) = outpos(:,2)+ULC_xcol-1;
Track(:,3) = outpos(:,3)+starttime-1;
Track(:,4) = outpos(:,4);
Track = Track(2:end,:); % To do not repeat two times the first point