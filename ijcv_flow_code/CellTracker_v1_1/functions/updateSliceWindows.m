function updateSliceWindows(slice, imagePrefix, handles)
% AUTHOR: Peter Horvath
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

global f_handles

% try

    numOfima = length(imfinfo(imagePrefix));
    if numOfima > 1
        f_handles.stopIm  = length(imfinfo(imagePrefix));
        set(f_handles.h_text1, 'String', ['Slide ' num2str(slice) '/' num2str(f_handles.stopIm)]);
        set(f_handles.h_slider1, 'Max', f_handles.stopIm);
        set(f_handles.h_slider1, 'Value', slice);
        set(f_handles.h_slider1, 'Min', f_handles.startIm);
        set(f_handles.h_slider1, 'SliderStep', [ 1/( f_handles.stopIm - f_handles.startIm) 5 /( f_handles.stopIm - f_handles.startIm)]);
        set(f_handles.h_slider1, 'Enable', 'on');
        f_handles.imageIndex = slice;
    end
    if numOfima >= 1
        if slice>numOfima
            reference = numOfima;
        else
            reference = slice;
        end
        name = imagePrefix;
        inputImage = imread(name, reference); 
        [row, col, ch] = size(inputImage);
        if ~isa(inputImage, 'uint8')
            inputImage = double(inputImage);
            inputImage = (inputImage - min(min(inputImage)));
            inputImage = inputImage ./ max(max(inputImage));
            inputImage = inputImage .* 256;
            inputImage = uint8(inputImage);
        end
        out(:,:,1) = inputImage(:,:,1);
        out(:,:,2) = inputImage(:,:,2);
        out(:,:,3) = inputImage(:,:,3);
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
        else
            pos = [];
        end

        if ~isempty(pos)
            if ~isempty(pos)
                cellsPresent = find(pos(:,3) == slice);
            else
                cellsPresent = [];
            end

            f_handles.cellIndex = double(inputImage * 0);

            for j=1:length(cellsPresent)
                xp = pos(cellsPresent(j), 1);
                yp = pos(cellsPresent(j), 2);
                if (xp > 10) && (xp < size(out, 1)-10)  && (yp > 10) && (yp < size(out, 2)-10)
                    colorr = mod(pos(cellsPresent(j), 4) * 1237, 255);
                    colorg = mod(pos(cellsPresent(j), 4) * 59, 255);
                    colorb = mod(pos(cellsPresent(j), 4) * 37, 255);

                    f_handles.cellIndex(xp-10:xp+10, yp-10:yp+10) = double(pos(cellsPresent(j), 4));

                    out(xp-10:xp+10, yp-10:yp-9, 1) = colorr;
                    out(xp-10:xp+10, yp+9:yp+10, 1) = colorr;
                    out(xp-10:xp-9, yp-10:yp+10, 1) = colorr;
                    out(xp+9:xp+10, yp-10:yp+10, 1) = colorr;

                    out(xp-10:xp+10, yp-10:yp-9, 2) = colorg;
                    out(xp-10:xp+10, yp+9:yp+10, 2) = colorg;
                    out(xp-10:xp-9, yp-10:yp+10, 2) = colorg;
                    out(xp+9:xp+10, yp-10:yp+10, 2) = colorg;

                    out(xp-10:xp+10, yp-10:yp-9, 3) = colorb;
                    out(xp-10:xp+10, yp+9:yp+10, 3) = colorb;
                    out(xp-10:xp-9, yp-10:yp+10, 3) = colorb;
                    out(xp+9:xp+10, yp-10:yp+10, 3) = colorb;
                end
            end
            for i=1:size(pos, 1)
                out(pos(i, 1), pos(i, 2), 1) = mod(pos(i, 4) * 1237, 255);
                out(pos(i, 1), pos(i, 2), 2) = mod(pos(i, 4) * 59, 255);
                out(pos(i, 1), pos(i, 2), 3) = mod(pos(i, 4) * 37, 255);
            end
        end
        imshow(uint8(out), 'Parent', f_handles.h_axes1);
    end

% catch ME1
%     Message = {'Could not perform operation (update slice window).', ...
%         ' '};
%     msgbox(Message,'Message')
end