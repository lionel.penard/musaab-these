function varargout = CellTrackerSettingsSemi(varargin)
% CELLTRACKERSETTINGSSEMI M-file for CellTrackerSettingsSemi.fig
%      CELLTRACKERSETTINGSSEMI, by itself, creates a new CELLTRACKERSETTINGSSEMI or raises the existing
%      singleton*.
%
%      H = CELLTRACKERSETTINGSSEMI returns the handle to a new CELLTRACKERSETTINGSSEMI or the handle to
%      the existing singleton*.
%
%      CELLTRACKERSETTINGSSEMI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERSETTINGSSEMI.M with the given input arguments.
%
%      CELLTRACKERSETTINGSSEMI('Property','Value',...) creates a new CELLTRACKERSETTINGSSEMI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerSettingsSemi_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerSettingsSemi_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTrackerSettingsSemi

% Last Modified by GUIDE v2.5 26-Jun-2015 16:29:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CellTrackerSettingsSemi_OpeningFcn, ...
    'gui_OutputFcn',  @CellTrackerSettingsSemi_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTrackerSettingsSemi is made visible.
function CellTrackerSettingsSemi_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerSettingsSemi (see VARARGIN)

global f_handles

f_handles.flagGUIclosedWithXrun = 1;

% Choose default command line output for CellTrackerSettingsSemi
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CellTrackerSettingsSemi wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%% set parameters to show
try
    
    % Close the other windows
    if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
    if ishandle(f_handles.h_GUIsetT); close(f_handles.h_GUIsetT); end;
    %if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetS); end;
    if ishandle(f_handles.h_GUIsetM); close(f_handles.h_GUIsetM); end;
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    
    % Method selection
    if f_handles.CellMatchingModality == 0
        set(handles.radiobutton1, 'Value', 1);
        set(handles.radiobutton2, 'Value', 0);
    else
        set(handles.radiobutton1, 'Value', 0);
        set(handles.radiobutton2, 'Value', 1);
    end
    
    inpImage = imread(f_handles.PathImgCurrent, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgCurrent));
    
    % Setting show
    v = floor(get(f_handles.h_slider1, 'Value'));
    set(handles.edit01, 'String', num2str(v));
    f_handles.TrackFirstPoint = v;
    set(handles.edit02, 'String', num2str(numImages));
    f_handles.TrackLastPoint = numImages;
    
    % Check parameters
    if f_handles.CellMaxDisp<1 || f_handles.CellMaxDisp>ceil(min([rowI, colI]))
        f_handles.CellMaxDisp = ceil(min([rowI, colI])/10);
    end
    if f_handles.CellTempSize<1 || f_handles.CellTempSize>ceil(min([rowI, colI]))
        f_handles.CellTempSize = ceil(min([rowI, colI])/10);
    end
    
    % Set parameters
    set(handles.edit03, 'String', num2str(f_handles.CellMaxDisp));
    set(handles.edit04, 'String', num2str(f_handles.CellTempSize));
    
catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerSettingsSemi_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global f_handles

if f_handles.flagGUIclosedWithXrun ~= 0;
    f_handles.flagGUIclosedWithX = 1;
end

delete(hObject);


% --- Executes on button press in pb_saveSettings.
function pb_saveSettings_Callback(hObject, eventdata, handles)
% hObject    handle to pb_saveSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% OK button to save settings
global f_handles;

f_handles.flagGUIclosedWithXrun = 0;

try
    if get(handles.radiobutton1, 'Value')== 1
        f_handles.CellMatchingModality = 0;
    else
        f_handles.CellMatchingModality = 1;
    end
      
    % Check parameters
    inpImage = imread(f_handles.PathImgCurrent, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgCurrent));

    MaxDisp    = str2num(get(handles.edit03, 'String'));
    TempSize   = str2num(get(handles.edit04, 'String'));
    flag_ComputationOn = 1;
    if MaxDisp<1 || MaxDisp>ceil(min([rowI, colI]))
        flag_ComputationOn = 0;
    end
    if TempSize<1 || TempSize>ceil(min([rowI, colI]))
        flag_ComputationOn = 0;
    end

    if flag_ComputationOn == 1
        f_handles.CellMaxDisp  = str2num(get(handles.edit03, 'String'));
        f_handles.CellTempSize = str2num(get(handles.edit04, 'String'));

        % Update visualization
        if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetS); end;

    else
        Message = {'Wrong parameter set.', ...
            ' '};
        msgbox(Message,'Message')
    end
    
    % Update visualization
    if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetMS); end;

catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function radiobutton1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1
global f_handles

if get(hObject,'Value') == 1
    set(handles.radiobutton2, 'Value', 0);
    f_handles.CellMatchingModality = 0;
else
    set(handles.radiobutton2, 'Value', 1);
    f_handles.CellMatchingModality = 1;
end


% --- Executes during object creation, after setting all properties.
function radiobutton2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2
global f_handles

if get(hObject,'Value') == 1
    set(handles.radiobutton1, 'Value', 0);
    f_handles.CellMatchingModality = 1;
else
    set(handles.radiobutton1, 'Value', 1);
    f_handles.CellMatchingModality = 0;
end


function edit01_Callback(hObject, eventdata, handles)
% hObject    handle to edit01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit01 as text
%        str2double(get(hObject,'String')) returns contents of edit01 as a double
global f_handles

f_handles.TrackFirstPoint = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function edit01_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit02_Callback(hObject, eventdata, handles)
% hObject    handle to edit02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit02 as text
%        str2double(get(hObject,'String')) returns contents of edit02 as a double
global f_handles;

f_handles.TrackLastPoint = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function edit02_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit03_Callback(hObject, eventdata, handles)
% hObject    handle to edit03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit03 as text
%        str2double(get(hObject,'String')) returns contents of edit03 as a double
global f_handles;

f_handles.CellMaxDisp = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function edit03_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit04_Callback(hObject, eventdata, handles)
% hObject    handle to edit04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit04 as text
%        str2double(get(hObject,'String')) returns contents of edit04 as a double
global f_handles

f_handles.CellTempSize = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function edit04_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
