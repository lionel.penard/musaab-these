function varargout = CellTrackerSettingsTracking(varargin)
% CELLTRACKERSETTINGSTRACKING M-file for CellTrackerSettingsTracking.fig
%      CELLTRACKERSETTINGSTRACKING, by itself, creates a new CELLTRACKERSETTINGSTRACKING or raises the existing
%      singleton*.
%
%      H = CELLTRACKERSETTINGSTRACKING returns the handle to a new CELLTRACKERSETTINGSTRACKING or the handle to
%      the existing singleton*.
%
%      CELLTRACKERSETTINGSTRACKING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERSETTINGSTRACKING.M with the given input arguments.
%
%      CELLTRACKERSETTINGSTRACKING('Property','Value',...) creates a new CELLTRACKERSETTINGSTRACKING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerSettingsTracking_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerSettingsTracking_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTrackerSettingsTracking

% Last Modified by GUIDE v2.5 29-Jun-2015 10:46:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CellTrackerSettingsTracking_OpeningFcn, ...
    'gui_OutputFcn',  @CellTrackerSettingsTracking_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTrackerSettingsTracking is made visible.
function CellTrackerSettingsTracking_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerSettingsTracking (see VARARGIN)

global f_handles

f_handles.flagGUIclosedWithXrun = 1;

% Choose default command line output for CellTrackerSettingsTracking
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CellTrackerSettingsTracking wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%% set parameters to show
try
    
    % Close the other windows
    if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
    if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetS); end;
    if ishandle(f_handles.h_GUIsetM); close(f_handles.h_GUIsetM); end;
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    
    inpImage = imread(f_handles.PathImgOriginal, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgOriginal));
    
    % Check parameters
    if f_handles.templateTreshold<1
        f_handles.templateTreshold = 1;
    end
    if f_handles.tracking.mem<1 || f_handles.tracking.mem>numImages
        f_handles.tracking.mem = ceil(numImages);
    end
    if f_handles.tracking.minlength<1 || f_handles.tracking.minlength>numImages
        f_handles.tracking.minlength = ceil(numImages);
    end
    if f_handles.tracking.stepsize<0 || f_handles.tracking.stepsize>ceil(min([rowI, colI]))
        f_handles.tracking.stepsize = ceil(min([rowI, colI]));
    end

    % Set parameters
    set(handles.edit4, 'String', num2str(f_handles.templateTreshold));
    set(handles.edit5, 'String', num2str(f_handles.tracking.mem));
    set(handles.edit6, 'String', num2str(f_handles.tracking.minlength));
    set(handles.edit7, 'String', num2str(f_handles.tracking.stepsize));

catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end;


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerSettingsTracking_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global f_handles

if f_handles.flagGUIclosedWithXrun ~= 0;
    f_handles.flagGUIclosedWithX = 1;
end

delete(hObject);


% --- Executes on button press in pb_saveSettings.
function pb_saveSettings_Callback(hObject, eventdata, handles)
% hObject    handle to pb_saveSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% OK button to save settings
global f_handles

f_handles.flagGUIclosedWithXrun = 0;

try
      
    % Errors check
    inpImage = imread(f_handles.PathImgOriginal, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgOriginal));
    testTemplateTreshold    = str2num(get(handles.edit4, 'String'));
    testTrackingMem         = str2num(get(handles.edit5, 'String'));
    testMinlength           = str2num(get(handles.edit6, 'String'));
    testStepsize            = str2num(get(handles.edit7, 'String'));
    flag_ComputationOn = 1;
    if testTemplateTreshold<1
        flag_ComputationOn = 0;
    end
    if testTrackingMem<1 || testTrackingMem>numImages
        flag_ComputationOn = 0;
    end
    if testMinlength<1 || testMinlength>numImages
        flag_ComputationOn = 0;
    end
    if testStepsize<0 || testStepsize>min([rowI, colI])
        flag_ComputationOn = 0;
    end
    
    if flag_ComputationOn == 1

        f_handles.templateTreshold   = str2num(get(handles.edit4, 'String'));
        f_handles.tracking.mem       = str2num(get(handles.edit5, 'String'));
        f_handles.tracking.minlength = str2num(get(handles.edit6, 'String'));
        f_handles.tracking.stepsize  = str2num(get(handles.edit7, 'String'));

        % Reset GUI
        set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
        
        % Update visualization
        if ishandle(f_handles.h_GUIsetT); close(f_handles.h_GUIsetT); end;
    else
        Message = {'Wrong parameter set.', ...
            ' '};
        msgbox(Message,'Message')
    end
catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end