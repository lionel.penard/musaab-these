function [CultureData, CultureDataColLabels, CellsData, CellsDataColLabels, TimepointsData, TimepointsDataColLabels] = statisticsComputation(TableNx4, timeUnitString, timeUnitValue, lengthUnitString, lengthUnitValue)
% AUTHOR: Filippo Piccinini (E-mail: f.piccinini@unibo.it)
% DATE: July 07, 2014
% NAME: statisticsComputation (version 1.0)
% 
% To compute some statistics of tracks in a table.
%
% PARAMETERS:
%
% 	TableNx4            Table with N rows and 4 columns where every row
%                       repots [yrow_coordinate, xcol_coordinate, 
%                       timepoint, cellID] of a track computed.
%
%   timeUnitString      String reporting in English the unit used to 
%                       measure the time elapsed between the acquisition  
%                       of two subsequent frames.
%
%   timeUnitValue       Time elapsed between the acquisition fo two
%                       subsequent frames measured in "timeUnitString".
%
%   lengthUnitString    String reporting in English the unit used to 
%                       measure the pixel size.
%
%   lengthUnitValue     Pixel size measured in "lengthUnitString".
%
%
% OUTPUT:
%
%   CultureData         Statistics computed considering all the cells 
%                       tracked together (statistics of the population).
%
%   CultureDataColLabels	Labels of the columns of the matrix.
%
%   CellsData           Statistics computed for every cell, considering 
%                       all the timepoint data available. 
%
%   CellsDataColLabels      Labels of the columns of the matrix.
%
%   TimepointsData      Statistics for every timepoint (distance,speed...)
%
%   TimepointsDataColLabels	Labels of the columns of the matrix.
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

    %% Settings

    [row, col, ch] = size(TableNx4);

    Col4TableNx4 = TableNx4(:,4);
    ids = sort(unique(Col4TableNx4));
    length_ids = length(ids);

    Col3TableNx4 = TableNx4(:,3);
    timesp = sort(unique(Col3TableNx4));
    length_timesp = length(timesp);

    %% Output Labels

    TimepointsDataColLabels = {[' y-coordinate [pixel] '], ...
        [' x-coordinate [pixel] '], ...
        [' Frame number '], ...
        [' Cell ID '], ...
        [' Distance from origin [' lengthUnitString '] '], ...
        [' Distance from last point [' lengthUnitString '] '], ...
        [' Instantaneous speed [' lengthUnitString '/' timeUnitString '] '], ...
        [' Angle from origin [degree] '], ...
        [' Angle from last point [degree] ']};

    CellsDataColLabels = {[' Cell ID '], ...
        [' Length total way run [' lengthUnitString '] '], ...
        [' Maximum distance from origin [' lengthUnitString '] '], ...
        [' Average distance from origin [' lengthUnitString '] '], ...
        [' Maximum distance from last point [' lengthUnitString '] '], ...
        [' Average distance from last point [' lengthUnitString '] '], ...
        [' Maximum speed [' lengthUnitString '/' timeUnitString '] '], ...
        [' Average speed [' lengthUnitString '/' timeUnitString '] '], ...
        [' Average angle of direction from origin [degree] '], ...
        [' Average angle of direction from last point [degree] ']};

    CultureDataColLabels = {[' Frame number '], ...
        [' Average distance from origin [' lengthUnitString '] '], ...
        [' Average speed [' lengthUnitString '/' timeUnitString '] '], ...
        [' Average angle direction from origin [degree] ']};


    %% Computation

    % Initial settings
    TimepointsData = [];
    CellsData = NaN*ones(length_ids, length(CellsDataColLabels));
    CultureData = NaN*ones(length_timesp, length(CultureDataColLabels));

    % Processing TimepointsData CellsData

    for i = 1:length_ids
        rowsRelatedToIDi = find(Col4TableNx4==ids(i));
        matrixIDi = TableNx4(rowsRelatedToIDi, :);
        matrixIDiOut = double(matrixIDi);
        length_times = size(matrixIDi,1);
        for j = 1:length_times
            if j == 1
                OriginPointHead = [matrixIDi(1,2), matrixIDi(1,1)];
                NewPointHead = [matrixIDi(1,2), matrixIDi(1,1)];
                matrixIDiOut(j,5) = NaN; %' Distance from origin'
                matrixIDiOut(j,6) = NaN; %' Distance from last point '
                matrixIDiOut(j,7) = NaN; %' Speed from last point ', 
                matrixIDiOut(j,8) = NaN; %' Angle from origin '
                matrixIDiOut(j,9) = NaN; %' Angle from last point '
            else
                NewPointHead = [matrixIDi(j,2), matrixIDi(j,1)];
                [VectorAngle_Last, VectorModule_Last, Vector_xcol_yrow_Last] = relativeVectorModuleAngle(LastPointHead, NewPointHead);
                [VectorAngle_Ori, VectorModule_Ori, Vector_xcol_yrow_Ori] = relativeVectorModuleAngle(OriginPointHead, NewPointHead);
                matrixIDiOut(j,5) = VectorModule_Ori*lengthUnitValue; %' Distance from origin'
                matrixIDiOut(j,6) = VectorModule_Last*lengthUnitValue; %' Distance from last point '
                matrixIDiOut(j,7) = VectorModule_Last*lengthUnitValue/(((matrixIDi(j,3)-1)*timeUnitValue)-((matrixIDi(j-1,3)-1)*timeUnitValue)); %' Speed from last point ', 
                matrixIDiOut(j,8) = floor(VectorAngle_Ori); %' Angle from origin '
                matrixIDiOut(j,9) = floor(VectorAngle_Last); %' Angle from last point '
            end
            LastPointHead = NewPointHead;
            clear NewPointHead
        end
        TimepointsData = [TimepointsData; matrixIDiOut];

        CellsData(i, 1) = ids(i); % Cell ID
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 6);
        if ~isempty(extractMat); CellsData(i, 2) = sum(extractMat(~isnan(extractMat))); end; % Length total way run [pixel]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 5);
        if ~isempty(extractMat); CellsData(i, 3) = max(extractMat(~isnan(extractMat))); end; % Maximum distance from origin [pixel]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 5);
        if ~isempty(extractMat); CellsData(i, 4) = mean(extractMat(~isnan(extractMat))); end; % Average distance from origin [pixel]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 6);
        if ~isempty(extractMat); CellsData(i, 5) = max(extractMat(~isnan(extractMat))); end; % Maximum distance from last point [pixel]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 6);
        if ~isempty(extractMat); CellsData(i, 6) = mean(extractMat(~isnan(extractMat))); end; % Average distance from last point [pixel]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 7);
        if ~isempty(extractMat); CellsData(i, 7) = max(extractMat(~isnan(extractMat))); end; % Maximum speed from last point [pixel/frame]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 7);
        if ~isempty(extractMat); CellsData(i, 8) = mean(extractMat(~isnan(extractMat))); end; % Average speed from last point [pixel/frame]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 8);
        if ~isempty(extractMat); CellsData(i, 9) = floor(meanAngle(extractMat(~isnan(extractMat)))); end; % Average angle of direction from origin [degree]
        clear extractMat; extractMat = matrixIDiOut(2:length_times, 9);
        if ~isempty(extractMat); CellsData(i, 10) = floor(meanAngle(extractMat(~isnan(extractMat)))); end; % Average angle of direction from last point [degree]
        clear matrixIDiOut matrixIDi rowsRelatedToIDi
    end

    % Processing CultureData
    for t = 1:length_timesp
        rowsTime = find(TimepointsData(:,3)==timesp(t));
        CultureData(t, 1) = timesp(t); % Frame number
        clear extractMat; extractMat = TimepointsData(rowsTime, 5);
        CultureData(t, 2) = mean(extractMat(~isnan(extractMat))); % Average distance from origin [pixel]
        clear extractMat; extractMat = TimepointsData(rowsTime, 7);
        CultureData(t, 3) = mean(extractMat(~isnan(extractMat))); % Average speed from last point [pixel/frame]
        clear extractMat; extractMat = TimepointsData(rowsTime, 8);
        CultureData(t, 4) = floor(meanAngle(extractMat(~isnan(extractMat)))); % Average angle direction [degree]
        clear rowsTime
    end
end

% From: http://rosettacode.org/wiki/Averages/Mean_angle
function u = meanAngle(vectorDegree)
	u = angle(mean(exp(i*pi*vectorDegree/180)))*180/pi;
    if u<0
        u = 360 + u;
    end
end