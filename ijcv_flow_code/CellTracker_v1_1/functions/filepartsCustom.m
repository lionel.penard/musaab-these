function [pathstr, fileNameexEx, ext, versn] = filepartsCustom(fileName)
% AUTHOR: Peter Horvath
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

[pathstr, fileNameexEx, ext] = fileparts(fileName);

versn = 1;