function varargout = CellTrackerFigureShow(varargin)
% CELLTRACKERFIGURESHOW MATLAB code for CellTrackerFigureShow.fig
%      CELLTRACKERFIGURESHOW, by itself, creates a new CELLTRACKERFIGURESHOW or raises the existing
%      singleton*.
%
%      H = CELLTRACKERFIGURESHOW returns the handle to a new CELLTRACKERFIGURESHOW or the handle to
%      the existing singleton*.
%
%      CELLTRACKERFIGURESHOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERFIGURESHOW.M with the given input arguments.
%
%      CELLTRACKERFIGURESHOW('Property','Value',...) creates a new CELLTRACKERFIGURESHOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerFigureShow_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerFigureShow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTrackerFigureShow

% Last Modified by GUIDE v2.5 16-Sep-2015 12:37:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CellTrackerFigureShow_OpeningFcn, ...
                   'gui_OutputFcn',  @CellTrackerFigureShow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTrackerFigureShow is made visible.
function CellTrackerFigureShow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerFigureShow (see VARARGIN)

% Choose default command line output for CellTrackerFigureShow
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Added by Filippo
global f_handles;
f_handles.h_text1 = handles.text1;
f_handles.h_FigureShow = handles.figure1;
f_handles.h_axes1 = handles.axes1;
f_handles.h_slider1 = handles.slider1;

% Check monitor screen size to set the initial figure
set(0,'units','characters')  
Pix_SS = get(0,'screensize');
gap = 20;
if min([Pix_SS(3), Pix_SS(4)])>2*gap
    set(f_handles.h_FigureShow,'Position',[10, 10, floor(Pix_SS(3)-gap), floor(Pix_SS(4)-gap)])
end


% UIWAIT makes CellTrackerFigureShow wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerFigureShow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
%% Shows seletced image
global f_handles;

v = floor(get(hObject,'Value'));
% I don't know why but sometimes if you manually move the slider up to one
% image and then you click on the button next of the slider it does not
% work. With these next few line of code I solved the problem.
if v == f_handles.lastIndexShown
    if f_handles.lastIndexShown ~= f_handles.lastImage
        v = v+1;
    end
end
f_handles.lastIndexShown = v;

guidata(hObject,handles);

set(f_handles.h_text1, 'String', ['Slide ' num2str(v)]);
f_handles.imageIndex = v;
guidata(hObject, handles);
updateSliceWindows(v, f_handles.PathImgCurrent, handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f_handles;

if ishandle(f_handles.h_GUIsetT); close(f_handles.h_GUIsetT); end;
if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;

set(f_handles.h_FCpb01, 'BackgroundColor', 'yellow');
set(f_handles.h_FCpb02, 'BackgroundColor', 'red');
set(f_handles.h_FCpb03, 'BackgroundColor', 'red');
set(f_handles.h_FCpb04, 'BackgroundColor', 'red');
set(f_handles.h_FCpb05, 'BackgroundColor', 'red');
set(f_handles.h_FCpb06, 'BackgroundColor', 'red');
set(f_handles.h_FCpb07, 'BackgroundColor', 'red');
% create tmp folder
if isequal(exist([pwd filesep 'tmpct'], 'dir'),7); rmdir([pwd filesep 'tmpct'],'s'); mkdir([pwd filesep 'tmpct']); else mkdir([pwd filesep 'tmpct']); end;
% Save output
path = [pwd filesep 'tmpct' filesep];
fileNameMat = 'TracksCoordinates.mat';
fileNameExcel = 'TracksCoordinates.xls';
pathFileNameMat = [path fileNameMat];
pathFileNameExcel = [path fileNameExcel];
if ~isempty(f_handles.pos)
    pos = f_handles.pos;
    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
    f_handles.pos = [];
else
    if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
    if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
end


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global f_handles;

try
    left_bottom_width_height = get(hObject, 'Position');
    f1_left = left_bottom_width_height(1);
    f1_bottom = left_bottom_width_height(2);
    f1_width = left_bottom_width_height(3);
    f1_height = left_bottom_width_height(4);

    set(f_handles.h_text1,'Position',[5,1,20,2]) %Values read from GUIDE
    set(f_handles.h_axes1,'Position',[5,5,f1_width-5-5,f1_height-5-1]) %Values read from GUIDE
    set(f_handles.h_slider1,'Position',[27,1,f1_width-27-5,2]) %Values read from GUIDE 
catch ME1
end
