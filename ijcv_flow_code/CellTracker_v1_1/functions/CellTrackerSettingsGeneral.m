function varargout = CellTrackerSettingsGeneral(varargin)
% CELLTRACKERSETTINGSGENERAL M-file for CellTrackerSettingsGeneral.fig
%      CELLTRACKERSETTINGSGENERAL, by itself, creates a new CELLTRACKERSETTINGSGENERAL or raises the existing
%      singleton*.
%
%      H = CELLTRACKERSETTINGSGENERAL returns the handle to a new CELLTRACKERSETTINGSGENERAL or the handle to
%      the existing singleton*.
%
%      CELLTRACKERSETTINGSGENERAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERSETTINGSGENERAL.M with the given input arguments.
%
%      CELLTRACKERSETTINGSGENERAL('Property','Value',...) creates a new CELLTRACKERSETTINGSGENERAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerSettingsGeneral_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerSettingsGeneral_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTrackerSettingsGeneral

% Last Modified by GUIDE v2.5 26-Jun-2015 16:36:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CellTrackerSettingsGeneral_OpeningFcn, ...
    'gui_OutputFcn',  @CellTrackerSettingsGeneral_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTrackerSettingsGeneral is made visible.
function CellTrackerSettingsGeneral_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerSettingsGeneral (see VARARGIN)

global f_handles

% Choose default command line output for CellTrackerSettingsGeneral
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CellTrackerSettingsGeneral wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%% set parameters to show
try
    
    % Close the other windows
    %if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
    if ishandle(f_handles.h_GUIsetT); close(f_handles.h_GUIsetT); end;
    if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetS); end;
    if ishandle(f_handles.h_GUIsetM); close(f_handles.h_GUIsetM); end;
    if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;
    
    inpImage = imread(f_handles.PathImgOriginal, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgOriginal));
    
    % background correction
    if f_handles.backCorrMethod == 1
        set(handles.radiobutton1, 'Value', 1);
    elseif  f_handles.backCorrMethod == 2
        set(handles.radiobutton2, 'Value', 1);
    end;
    
    % Check parameters
    if f_handles.backCorrSize<1 || f_handles.backCorrSize>=min([rowI, colI])
        f_handles.backCorrSize = 1;
    end
    if f_handles.maxAlignDistance<0 || f_handles.maxAlignDistance>=min([rowI, colI])
        f_handles.maxAlignDistance = ceil(min([rowI, colI])/10);
    end
    
    % Set parameters
    set(handles.edit1, 'String', num2str(f_handles.backCorrSize));

    % auto aligner
    set(handles.edit3, 'String', num2str(f_handles.maxAlignDistance));

catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerSettingsGeneral_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in pb_saveSettings.
function pb_saveSettings_Callback(hObject, eventdata, handles)
% hObject    handle to pb_saveSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% OK button to save settings
global f_handles;

try
    %Reset input
    f_handles.PathImgCurrent = f_handles.PathImgOriginal;
    f_handles.startIm = 1;
    f_handles.lastImage  = length(imfinfo(f_handles.PathImgOriginal));
    
    % Errors check
    inpImage = imread(f_handles.PathImgCurrent, 1);
    [rowI, colI, chI] = size(inpImage);
    numImages =  length(imfinfo(f_handles.PathImgCurrent));
    testBackCorrSize        = str2num(get(handles.edit1, 'String'));
    testMaxAlignDistance    = str2num(get(handles.edit3, 'String'));
    flag_ComputationOn = 1;
    if testBackCorrSize<1 || testBackCorrSize>=min([rowI, colI])
        flag_ComputationOn = 0;
    end
    if testMaxAlignDistance<0 || testMaxAlignDistance>=min([rowI, colI])
        flag_ComputationOn = 0;
    end
    
    if flag_ComputationOn == 1
        if  get(handles.radiobutton1, 'Value')== 1
            f_handles.backCorrMethod = 1;
        elseif get(handles.radiobutton2, 'Value') == 1
            f_handles.backCorrMethod = 2;
        end;
        f_handles.backCorrSize = str2num(get(handles.edit1, 'String'));

        % auto aligner
        f_handles.maxAlignDistance = str2num(get(handles.edit3, 'String'));

        % Reset GUI
        set(f_handles.h_FCpb02, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb03, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb04, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb05, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb06, 'BackgroundColor', 'yellow');
        set(f_handles.h_FCpb07, 'BackgroundColor', 'red');

        % Update visualization
        
        % Save output
        path = [pwd filesep 'tmpct' filesep];
        fileNameMat = 'TracksCoordinates.mat';
        fileNameExcel = 'TracksCoordinates.xls';
        pathFileNameMat = [path fileNameMat];
        pathFileNameExcel = [path fileNameExcel];
        if ~isempty(f_handles.pos)
            pos = f_handles.pos;
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); save(pathFileNameMat, 'pos'); else save(pathFileNameMat, 'pos'); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); xlswrite(pathFileNameExcel, pos); else xlswrite(pathFileNameExcel, pos); end;
            f_handles.pos = [];
        else
            if isequal(exist(pathFileNameMat, 'file'),2); delete(pathFileNameMat); end;
            if isequal(exist(pathFileNameExcel, 'file'),2); delete(pathFileNameExcel); end;
        end
        
        f_handles.PathImgCurrent = f_handles.PathImgOriginal;
        guidata(hObject, handles);
        updateSliceWindows(f_handles.startIm, f_handles.PathImgCurrent, handles);
        if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
    else
        Message = {'Wrong parameter set.', ...
            ' '};
        msgbox(Message,'Message')
    end
catch ME1
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end