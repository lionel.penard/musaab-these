function dist = matchHistograms(img, tmp, NumberOfBins, DistanceMetric, step)
% AUTHOR: Filippo Piccinini (E-mail: f.piccinini@unibo.it)
% DATE: 27 May 2015
% NAME: matchHistograms
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

% Parameters
if nargin < 3
    NumberOfBins = 32;
    DistanceMetric = 'euclidean';
    step = 10;
elseif nargin < 4
    DistanceMetric = 'euclidean';
    step = 10;
elseif nargin < 5
    step = 10;
end
   
% Internal settings
img = double(img);
tmp = double(tmp);
[ix, iy, ic] = size(img);
[tx, ty, ic] = size(tmp);
normMin = min([min(img(:)), min(tmp(:))]);
normMax = max([max(img(:)), max(tmp(:))]);
img01 = (img - normMin)/(normMax-normMin);
tmp01 = (tmp - normMin)/(normMax-normMin);

% Computation
LOW_HIGH = stretchlim(img01, [0.02 0.98]);
bins = linspace(LOW_HIGH(1), LOW_HIGH(2), NumberOfBins);
tmpHist = hist(tmp01(:), bins);
%figure(1); plot(tmpHist)
for i=1:step:ix-tx
    for j=1:step:iy-ty
        smallImg = img01(i:i+tx-1, j:j+ty-1);
        smallImgHist = hist(smallImg(:), bins);
        %figure(2); plot(smallImgHist)
        distSmall(i, j) = pdist([smallImgHist; tmpHist],DistanceMetric);   
    end
end
distSmall = (distSmall - min(distSmall(:)))/(max(distSmall(:))-min(distSmall(:)));
distSmall = -(distSmall - 1);

% Computation
dist = NaN.*ones(ix, iy);
txK = 1;
if mod(tx,2)
    txK = 0;
end
tyK = 1;
if mod(ty,2)
    tyK = 0;
end
dist(ceil(tx/2):end-ceil(tx/2)-txK, ceil(ty/2):end-ceil(ty/2)-tyK) = distSmall;