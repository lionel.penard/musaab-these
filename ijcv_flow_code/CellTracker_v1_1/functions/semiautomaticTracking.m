function outpos = semiautomaticTracking(x, y, startframe, inFile, startIm, stopIm, tempsize, maxdisp, backward, FlagHistogramMatching)
% AUTHOR: Peter Horvath
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

% This funciton tracks the object located at [x, y] in the startframe frame, backwards adn forwards optionally.
% load template image;

in = imread(inFile, startframe);
[sizeX, sizeY] = size(in);

if (x>tempsize+1) && (y>tempsize+1) && (x<sizeX-tempsize-1) && (x<sizeY-tempsize-1)
    tempImg = in(x-tempsize:x+tempsize, y-tempsize:y+tempsize);
end

currframe = startframe;
outpos = [x y startframe 1];

%h_wait = waitbar(0, 'Please wait...');
%steps = (stopIm-startframe + 1);
while (x>tempsize) && (x<sizeX - tempsize) && (y>tempsize) && (y<sizeY - tempsize) && (currframe < stopIm)
    %waitbar((currframe-startframe)/steps)
    
    currframe = currframe + 1;
    searchImgBig = imread(inFile, currframe);    
    
    % mirror boundary if nescessary
    mirrorflag = 0;
    if (x<tempsize+maxdisp + 2) || (x>sizeX - tempsize-maxdisp-2) || (y<tempsize+maxdisp+1) || (y>sizeY - tempsize-maxdisp-1)
        hxt = tempsize+maxdisp;
        hyt = tempsize+maxdisp;
        xi=sizeX; yi=sizeY;
        xt = tempsize; 
        yt = tempsize;
        mimg = zeros(sizeX + 2*hxt+1, sizeY + 2*hyt+1);
        mimg(hxt+1:hxt+xi, hyt+1:hyt+yi) = searchImgBig ;
        mimg(1:hxt, hyt+1:hyt+yi) = mimg(2*hxt:-1:hxt+1, hyt+1:hyt+yi);
        mimg(xi+hxt+1:xi+2*hxt, hyt+1:hyt+yi) = mimg(xi+hxt:-1:xi+1, hyt+1:hyt+yi);
        mimg(1:sizeX + 2*hxt+1, 1:hyt) = mimg(1:sizeX + 2*hxt+1, 2*hyt:-1:hyt+1);
        mimg(1:sizeX + 2*hxt+1, yi+hyt+1:yi+2*hyt) = mimg(1:sizeX + 2*hxt+1, yi+hyt:-1:yi+1);
        searchImg = mimg(x-tempsize-maxdisp+hxt:x+tempsize+maxdisp+hyt, y-tempsize-maxdisp+hxt:y+tempsize+maxdisp+hyt);
        mirrorflag = 1;
    else
        searchImg = searchImgBig(x-tempsize-maxdisp:x+tempsize+maxdisp, y-tempsize-maxdisp:y+tempsize+maxdisp);
    end
    [sizeIm, sizeIm] = size(searchImg);
    
    if FlagHistogramMatching == 1
        % Histogram matching (writen by Filippo)
        NumberOfBins = 10;
        DistanceMetric = 'euclidean';
        step = 1;
        corr2 = matchHistograms(searchImg, tempImg, NumberOfBins, DistanceMetric, step);
        corr2(isnan(corr2)) = 0;
        %figure(1); imagesc(corr2); drawnow; 
    else
        % Template matching (writen by Peter)
        corr = standardCorrelation(searchImg, tempImg, 0);
        %figure(2); imagesc(corr); drawnow; 
        H = fspecial('disk',6);
        corr2 = imfilter(corr,H);
    end
    
    % find max
    maxcorr = max(max(corr2));
    place = find(corr2 == maxcorr);
    place = place(1);
    ynew = floor(place / sizeIm);
    xnew = mod( place, sizeIm);
    xnew = xnew -tempsize-maxdisp;
    ynew = ynew -tempsize-maxdisp;
    x = round(x+xnew);
    y = round(y+ynew);
    if (x>tempsize+1) && (y>tempsize+1) && (x<sizeX-tempsize-1) && (y<sizeY-tempsize-1)
        outpos = [outpos'  [x y currframe 1]']';
        if ~mod(currframe , 5)
            tempImg = searchImgBig(x-tempsize:x+tempsize, y-tempsize:y+tempsize);
        end
    end
end