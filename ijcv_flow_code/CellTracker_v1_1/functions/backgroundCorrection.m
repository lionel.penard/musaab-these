function out = backgroundCorrection(inFile, startIm, stopIm, n, method)
% AUTHOR: Peter Horvath
 
% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

[xs, ys] = size(imread(inFile, startIm));
in = zeros(xs, ys, stopIm-startIm + 1, 'int16');

if n>xs || n>ys
    Message = {'The black side parameter must be lower than the image side.', ...
        ' '};
    msgbox(Message,'Message')
    return;
end

%% start the process
h_wait = waitbar(0, 'Please wait...');
steps = (stopIm-startIm + 1);
% try
    for i = 1:(stopIm-startIm + 1)  
        waitbar(i/steps)

        img = imread(inFile, i+startIm-1);
        in(:, :, i) = adaptivetresh(img, n, method);    
    end
% catch ME1
%     if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end; 
%     
%     Message = {'Could not perform this vignetting correction. Try to change correction method.', ...
%         ' '};
%     msgbox(Message,'Message')
%     return;
% end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end; 

%scale the data to 0-255
mini = min(in(:));
in = in - mini;
maxi = max(in(:)); maxi = double(maxi);
[pathstr, fileName, ext] = fileparts(inFile);
fileName = [pwd filesep 'tmpct' filesep fileName '_bc.tif'];

h_wait = waitbar(0, 'Please wait...');
steps = (stopIm-startIm + 1);
for i=1:(stopIm-startIm + 1)
    waitbar(i/steps)
    out = in(:,:,i);
    out = double(out);
    out = (out ./ maxi) * 255;
    out = uint8(out);   
    if i==1
        imwrite(out, fileName);
    else
        imwrite(out, fileName, 'WriteMode', 'append');
    end
end
if exist('h_wait'); if ishandle(h_wait); close(h_wait); end; end;


function out = adaptivetresh(in, n, method)

[SizeX, SizeY] = size(in);
in = double(in);
if method == 1
    small = imresize(in, 1/n);
    out2 = imresize(small, [SizeX SizeY]);
elseif method == 2
%     try
        out = in;
        counter = 1;
        for i=1:floor(SizeX/n)+1
            for j=1:floor(SizeY/n)+1
                XLower = (i-1)*n+1;
                XUpper = (i-1)*n+n;
                YLower = (j-1)*n+1;
                YUpper = (j-1)*n+n;
                if XUpper > SizeX
                    XUpper = SizeX;
                end
                if YUpper > SizeY
                    YUpper = SizeY;
                end
                places(1, counter) = (XUpper + XLower)/2;
                places(2, counter) = (YUpper + YLower)/2;
                avg = mean(mean(in(XLower:XUpper, YLower:YUpper)));
                values(counter) = avg;
                counter = counter + 1;
            end;
        end;
        
        approxFun = tpaps(places,values);

        counter=1;
        places = zeros(SizeX*SizeY, 2);
        for i=1:SizeX
            for j=1:SizeY
                places(counter, 1) = i;
                places(counter, 2) = j;
                counter = counter + 1;
            end;
        end;
        vals = fnval(approxFun, places');
        counter = 1;
        for i=1:SizeX
            for j=1:SizeY
                out2(i, j)=vals(counter);
                counter = counter + 1;
            end;
        end;
%     catch ME1
%         Message = {'Could not perform background subtraction using TPS, make sure you have the Spline Fitting Toolbox!', ...
%             ' '};
%         msgbox(Message,'Message')
%         return;
%     end
    
end
out = in - out2;