function [lociImgPointer lociImageProps] = lociLoadHeader(fileName)
% AUTHOR: Peter Horvath
%
% This function partially inherited from the standard bioformats bfopen function

% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

lociImgPointer = loci.formats.ChannelFiller();
lociImgPointer = loci.formats.ChannelSeparator(lociImgPointer);
lociImgPointer.setMetadataStore(loci.formats.MetadataTools.createOMEXMLMetadata());
lociImgPointer.setId(fileName);

numSeries = lociImgPointer.getSeriesCount();

lociImgPointer.setSeries(0);
lociImageProps.width = lociImgPointer.getSizeX();
lociImageProps.height = lociImgPointer.getSizeY();
lociImageProps.pixelType = lociImgPointer.getPixelType();
lociImageProps.bpp = loci.formats.FormatTools.getBytesPerPixel(lociImageProps.pixelType);
lociImageProps.fp = loci.formats.FormatTools.isFloatingPoint(lociImageProps.pixelType);
lociImageProps.sgn = loci.formats.FormatTools.isSigned(lociImageProps.pixelType);
lociImageProps.bppMax = power(2, lociImageProps.bpp * 8);
lociImageProps.little = lociImgPointer.isLittleEndian();
lociImageProps.numImages = lociImgPointer.getImageCount();

% check MATLAB version, since typecast function requires MATLAB 7.1+
lociImageProps.canTypecast = versionCheck(version, 7, 1);

% check Bio-Formats version, since makeDataArray2D function requires trunk
lociImageProps.bioFormatsVersion = char(loci.formats.FormatTools.VERSION);
lociImageProps.isBioFormatsTrunk = versionCheck(lociImageProps.bioFormatsVersion, 5, 0);


function [result] = versionCheck(v, maj, min)

tokens = regexp(v, '[^\d]*(\d+)[^\d]+(\d+).*', 'tokens');
majToken = tokens{1}(1);
minToken = tokens{1}(2);
major = str2num(majToken{1});
minor = str2num(minToken{1});
result = major > maj || (major == maj && minor >= min);
