function lociImage = lociLoadImage(lociImgPointer, lociImageProps, frameNumber)
% AUTHOR: Peter Horvath
%
% This function partially inherited from the standard bioformats bfopen function

% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it 
% under the terms of the GNU General Public License version 3 (or higher) 
% as published by the Free Software Foundation. This program is 
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
% General Public License for more details.

% check if the frameNumber is in the range
if frameNumber > 0 && frameNumber <= lociImageProps.numImages
    
        plane = lociImgPointer.openBytes(frameNumber - 1);

        % retrieve color map data
        if lociImageProps.bpp == 1
            colorMaps = lociImgPointer.get8BitLookupTable()';
        else
            colorMaps = lociImgPointer.get16BitLookupTable()';
        end
         
         warning off
         if ~isempty(colorMaps)
             newMap = colorMaps;
             m = newMap < 0;
             newMap(m) = newMap(m) + lociImageProps.bppMax;
             colorMaps = newMap / (lociImageProps.bppMax - 1);
         end
         warning on
 
         % convert byte array to MATLAB image
         if lociImageProps.isBioFormatsTrunk && (lociImageProps.sgn || ~lociImageProps.canTypecast)
             % can get the data directly to a matrix
             arr = loci.common.DataTools.makeDataArray2D(plane, ...
                 lociImageProps.bpp, lociImageProps.fp, lociImageProps.little, lociImageProps.height);
         else
             % get the data as a vector, either because makeDataArray2D
             % is not available, or we need a vector for typecast
             arr = loci.common.DataTools.makeDataArray(plane, ...
                 lociImageProps.bpp, lociImageProps.fp, lociImageProps.little);
         end
 
         % Java does not have explicitly unsigned data types;
         % hence, we must inform MATLAB when the data is unsigned
         if ~lociImageProps.sgn
             if lociImageProps.canTypecast
                 % TYPECAST requires at least MATLAB 7.1
                 % NB: arr will always be a vector here
                 switch class(arr)
                     case 'int8'
                         arr = typecast(arr, 'uint8');
                     case 'int16'
                         arr = typecast(arr, 'uint16');
                     case 'int32'
                         arr = typecast(arr, 'uint32');
                     case 'int64'
                         arr = typecast(arr, 'uint64');
                 end
             else
                 % adjust apparent negative values to actual positive ones
                 % NB: arr might be either a vector or a matrix here
                 mask = arr < 0;
                 adjusted = arr(mask) + lociImageProps.bppMax / 2;
                 switch class(arr)
                     case 'int8'
                         arr = uint8(arr);
                         adjusted = uint8(adjusted);
                     case 'int16'
                         arr = uint16(arr);
                         adjusted = uint16(adjusted);
                     case 'int32'
                         arr = uint32(arr);
                         adjusted = uint32(adjusted);
                     case 'int64'
                         arr = uint64(arr);
                         adjusted = uint64(adjusted);
                 end
                 adjusted = adjusted + lociImageProps.bppMax / 2;
                 arr(mask) = adjusted;
             end
         end
 
         if isvector(arr)
             % convert results from vector to matrix
             shape = [lociImageProps.width lociImageProps.height];
             lociImage = reshape(arr, shape)';
         end
    
else
   
    disp('Opps, the desired image is less than 1 or bigger than the last image in the sequence!');
    lociImage = [];
    
end