function varargout = CellTrackerStatistics(varargin)
% CELLTRACKERSTATISTICS MATLAB code for CellTrackerStatistics.fig
%      CELLTRACKERSTATISTICS, by itself, creates a new CELLTRACKERSTATISTICS or raises the existing
%      singleton*.
%
%      H = CELLTRACKERSTATISTICS returns the handle to a new CELLTRACKERSTATISTICS or the handle to
%      the existing singleton*.
%
%      CELLTRACKERSTATISTICS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKERSTATISTICS.M with the given input arguments.
%
%      CELLTRACKERSTATISTICS('Property','Value',...) creates a new CELLTRACKERSTATISTICS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTrackerStatistics_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTrackerStatistics_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Added by Filippo
addpath([pwd filesep 'functions'])

% Edit the above text to modify the response to help CellTrackerStatistics

% Last Modified by GUIDE v2.5 01-Jun-2015 09:16:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CellTrackerStatistics_OpeningFcn, ...
                   'gui_OutputFcn',  @CellTrackerStatistics_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTrackerStatistics is made visible.
function CellTrackerStatistics_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTrackerStatistics (see VARARGIN)

global f_handles
global s_handles

% Close the other windows
if ishandle(f_handles.h_GUIsetG); close(f_handles.h_GUIsetG); end;
if ishandle(f_handles.h_GUIsetT); close(f_handles.h_GUIsetT); end;
if ishandle(f_handles.h_GUIsetS); close(f_handles.h_GUIsetS); end;
if ishandle(f_handles.h_GUIsetM); close(f_handles.h_GUIsetM); end;
%if ishandle(f_handles.h_GUIstatistics); close(f_handles.h_GUIstatistics); end;

s_handles.timeUnitString = 'frame';
s_handles.timeUnitValue = 1;
s_handles.lengthUnitString = 'pixel';
s_handles.lengthUnitValue = 1;

s_handles.pos = [];
if ~isempty(varargin)
    pos = varargin{1};
    [row, col, ch] = size(pos);
    if col==4
        s_handles.pos = pos;
        clear pos
    else
        s_handles.pos = [];
    end
end

% Choose default command line output for CellTrackerStatistics
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CellTrackerStatistics wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CellTrackerStatistics_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


%% INPUT PARAMETER QUESTIONS


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global s_handles

try
    edit1value = str2double(get(hObject,'String'));
    if isnan(edit1value) || edit1value<=0 || isempty(edit1value)
        error('Wrong parameter set.')
    end
    s_handles.timeUnitValue = edit1value;
catch ME1
    set(handles.edit1,'String','1')
    s_handles.timeUnitValue = 1;
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global s_handles

try
    edit2value = str2double(get(hObject,'String'));
    if isnan(edit2value) || edit2value<=0 || isempty(edit2value)
        error('Wrong parameter set.')
    end
    s_handles.lengthUnitValue = edit2value;
catch ME1
    set(handles.edit2,'String','1')
    s_handles.lengthUnitValue = 1;
    Message = {'Wrong parameter set.', ...
        ' '};
    msgbox(Message,'Message')
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
global s_handles

contents = cellstr(get(hObject,'String'));
unit = contents{get(hObject,'Value')};
if ~strcmpi(unit,contents{1})
    set(handles.edit1, 'enable', 'on');
    edit1_Callback(handles.edit1, eventdata, handles);
    s_handles.timeUnitString = unit;
else
    set(handles.edit1, 'enable', 'off');
    set(handles.edit1, 'String', '1');
    s_handles.timeUnitValue = 1;
    s_handles.timeUnitString = 'frame';
end


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
global s_handles

contents = cellstr(get(hObject,'String'));
unit = contents{get(hObject,'Value')};
if ~strcmpi(unit,contents{1})
    set(handles.edit2, 'enable', 'on')
    edit2_Callback(handles.edit2, eventdata, handles);
    s_handles.lengthUnitString = unit;
else
    set(handles.edit2, 'enable', 'off');
    set(handles.edit2, 'String', '1');
    s_handles.lengthUnitValue = 1;
    s_handles.lengthUnitString = 'pixel';
end

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% CHECK BUTTONS


% --- Executes on button press in cb_01_01.
function cb_01_01_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_01


% --- Executes on button press in cb_01_02.
function cb_01_02_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_02


% --- Executes on button press in cb_01_03.
function cb_01_03_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_03


% --- Executes on button press in cb_01_04.
function cb_01_04_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_04


% --- Executes on button press in cb_01_05.
function cb_01_05_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_05 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_05


% --- Executes on button press in cb_01_06.
function cb_01_06_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_06 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_06


% --- Executes on button press in cb_01_07.
function cb_01_07_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_07 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_07


% --- Executes on button press in cb_01_08.
function cb_01_08_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_08 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_08


% --- Executes on button press in cb_01_09.
function cb_01_09_Callback(hObject, eventdata, handles)
% hObject    handle to cb_01_09 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_01_09


% --- Executes on button press in cb_02_01.
function cb_02_01_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_01

% --- Executes on button press in cb_02_02.
function cb_02_02_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_02


% --- Executes on button press in cb_02_03.
function cb_02_03_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_03


% --- Executes on button press in cb_02_04.
function cb_02_04_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_04


% --- Executes on button press in cb_02_05.
function cb_02_05_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_05 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_05


% --- Executes on button press in cb_02_06.
function cb_02_06_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_06 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_06


% --- Executes on button press in cb_02_07.
function cb_02_07_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_07 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_07


% --- Executes on button press in cb_02_08.
function cb_02_08_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_08 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_08


% --- Executes on button press in cb_02_09.
function cb_02_09_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_09 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_09


% --- Executes on button press in cb_02_10.
function cb_02_10_Callback(hObject, eventdata, handles)
% hObject    handle to cb_02_10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_02_10


% --- Executes on button press in cb_03_01.
function cb_03_01_Callback(hObject, eventdata, handles)
% hObject    handle to cb_03_01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_03_01


% --- Executes on button press in cb_03_02.
function cb_03_02_Callback(hObject, eventdata, handles)
% hObject    handle to cb_03_02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_03_02


% --- Executes on button press in cb_03_03.
function cb_03_03_Callback(hObject, eventdata, handles)
% hObject    handle to cb_03_03 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_03_03


% --- Executes on button press in cb_03_04.
function cb_03_04_Callback(hObject, eventdata, handles)
% hObject    handle to cb_03_04 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_03_04


%% COMPUTE STATISTICS


% --- Executes during object creation, after setting all properties.
function pushbutton1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s_handles

popupmenu1_Callback(handles.popupmenu1, eventdata, handles);
popupmenu2_Callback(handles.popupmenu2, eventdata, handles);

cb_01_01_Value = get(handles.cb_01_01,'Value');
cb_01_02_Value = get(handles.cb_01_02,'Value');
cb_01_03_Value = get(handles.cb_01_03,'Value');
cb_01_04_Value = get(handles.cb_01_04,'Value');
cb_01_05_Value = get(handles.cb_01_05,'Value');
cb_01_06_Value = get(handles.cb_01_06,'Value');
cb_01_07_Value = get(handles.cb_01_07,'Value');
cb_01_08_Value = get(handles.cb_01_08,'Value');
cb_01_09_Value = get(handles.cb_01_09,'Value');

cb_02_01_Value = get(handles.cb_02_01,'Value');
cb_02_02_Value = get(handles.cb_02_02,'Value');
cb_02_03_Value = get(handles.cb_02_03,'Value');
cb_02_04_Value = get(handles.cb_02_04,'Value');
cb_02_05_Value = get(handles.cb_02_05,'Value');
cb_02_06_Value = get(handles.cb_02_06,'Value');
cb_02_07_Value = get(handles.cb_02_07,'Value');
cb_02_08_Value = get(handles.cb_02_08,'Value');
cb_02_09_Value = get(handles.cb_02_09,'Value');
cb_02_10_Value = get(handles.cb_02_10,'Value');

cb_03_01_Value = get(handles.cb_03_01,'Value');
cb_03_02_Value = get(handles.cb_03_02,'Value');
cb_03_03_Value = get(handles.cb_03_03,'Value');
cb_03_04_Value = get(handles.cb_03_03,'Value');

flag_cb01 = [cb_01_01_Value, cb_01_02_Value, cb_01_03_Value, cb_01_04_Value, cb_01_05_Value, cb_01_06_Value, cb_01_07_Value, cb_01_08_Value, cb_01_09_Value];
flag_cb02 = [cb_02_01_Value, cb_02_02_Value, cb_02_03_Value, cb_02_04_Value, cb_02_05_Value, cb_02_06_Value, cb_02_07_Value, cb_02_08_Value, cb_02_09_Value, cb_02_10_Value];
flag_cb03 = [cb_03_01_Value, cb_03_02_Value, cb_03_03_Value, cb_03_04_Value];

try
    if isempty(s_handles.pos)
        Message = {'No track available.', ...
            ' '};
        msgbox(Message,'Message')
    else
        clear pos
        pos = s_handles.pos;
        pos = sortrows(pos, [4 3]);
        
        [CultureData, CultureDataColLabels, CellsData, CellsDataColLabels, TimepointsData, TimepointsDataColLabels] = statisticsComputation(pos, s_handles.timeUnitString, s_handles.timeUnitValue, s_handles.lengthUnitString, s_handles.lengthUnitValue);
        
        % Data for every timepoint
        if sum(flag_cb01)>0
            TimepointsDataSelected          = TimepointsData(:, find(flag_cb01==1));
            TimepointsDataColLabelsSelected = TimepointsDataColLabels(find(flag_cb01==1));
            showTableData(TimepointsDataSelected, TimepointsDataColLabelsSelected, 'DATA OF THE SINGLE TIMEPOINTS (for every cell tracked)')
        end
        
        % Data for every cell tracked
        if sum(flag_cb02)>0
            CellsDataSelected          = CellsData(:, find(flag_cb02==1));
            CellsDataColLabelsSelected = CellsDataColLabels(find(flag_cb02==1));
            showTableData(CellsDataSelected, CellsDataColLabelsSelected, 'DATA OF THE SINGLE CELLS TRACKED')
        end
        
        % Global Data of the culture
        if sum(flag_cb03)>0
            CultureDataSelected          = CultureData(:, find(flag_cb03==1));
            CultureDataColLabelsSelected = CultureDataColLabels(find(flag_cb03==1));
            showTableData(CultureDataSelected, CultureDataColLabelsSelected, 'DATA OF THE CULTURE (considering all the cells)')
        end
        
        if sum(flag_cb01)==0 && sum(flag_cb02)==0 && sum(flag_cb03)==0
            Message = {'No feature selected.', ...
                ' '};
            msgbox(Message,'Message')
        end
    end
catch ME1
    Message = {'Statistics computation fails.', ...
        ' '};
    msgbox(Message,'Message')
end


%% OLD FUNCTIONS NOT USED

% % --- Executes on button press in checkbox21.
% function checkbox21_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox21 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox21
% 
% 
% % --- Executes on button press in checkbox22.
% function checkbox22_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox22 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox22
% 
% 
% % --- Executes on button press in checkbox23.
% function checkbox23_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox23 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox23
% 
% 
% % --- Executes on button press in checkbox12.
% function checkbox12_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox12 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox12
% 
% 
% % --- Executes on button press in checkbox13.
% function checkbox13_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox13 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox13
% 
% 
% % --- Executes on button press in checkbox14.
% function checkbox14_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox14 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox14
% 
% 
% % --- Executes on button press in checkbox15.
% function checkbox15_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox15 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox15
% 
% 
% % --- Executes on button press in checkbox16.
% function checkbox16_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox16 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox16
% 
% 
% % --- Executes on button press in checkbox17.
% function checkbox17_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox17 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox17
% 
% 
% % --- Executes on button press in checkbox18.
% function checkbox18_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox18 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox18
% 
% 
% % --- Executes on button press in checkbox19.
% function checkbox19_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox19 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox19
% 
% 
% % --- Executes on button press in checkbox20.
% function checkbox20_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox20 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox20
% 
% 
% % --- Executes on button press in checkbox1.
% function checkbox1_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox1
