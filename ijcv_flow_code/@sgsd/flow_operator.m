function [A, b, params, iterative] = flow_operator(this, uv, duv, It, Ix, Iy)
%FLOW_OPERATOR   Linear flow operator (equation) for flow estimation
%   [A, b] = FLOW_OPERATOR(THIS, UV, INIT)  
%   returns a linear flow operator (equation) of the form A * x = b.  The
%   flow equation is linearized around UV with the initialization INIT
%   (e.g. from a previous pyramid level).  
%
%  
%   This is a member function of the class 'SGSD_optical_flow'. 
%
%   Author: Deqing Sun, Department of Computer Science, Brown University
%   Contact: dqsun@cs.brown.edu
%   $Date: 2007-11-30 $
%
% Copyright 2007-2010, Brown University, Providence, RI. USA
% 
%                          All Rights Reserved
% 
% All commercial use of this software, whether direct or indirect, is
% strictly prohibited including, without limitation, incorporation into in
% a commercial product, use in a commercial service, or production of other
% artifacts for commercial purposes.     
%
% Permission to use, copy, modify, and distribute this software and its
% documentation for research purposes is hereby granted without fee,
% provided that the above copyright notice appears in all copies and that
% both that copyright notice and this permission notice appear in
% supporting documentation, and that the name of the author and Brown
% University not be used in advertising or publicity pertaining to
% distribution of the software without specific, written prior permission.        
%
% For commercial uses contact the Technology Venture Office of Brown University
% 
% THE AUTHOR AND BROWN UNIVERSITY DISCLAIM ALL WARRANTIES WITH REGARD TO
% THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
% FITNESS FOR ANY PARTICULAR PURPOSE.  IN NO EVENT SHALL THE AUTHOR OR
% BROWN UNIVERSITY BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
% DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
% PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
% ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
% THIS SOFTWARE.  

    

   %This version is modified by Musaab to code SGSD model based on Deging Sun overall framework.
   
   
   
   
%image size and number of pixels
  sz        = [size(Ix,1) size(Ix,2)];
   npixels   = prod(sz);

  
  % spatial term
  S = this.spatial_filters;
  FU = sparse(npixels, npixels);
  FV = sparse(npixels, npixels);
   
  L     = [0 1 0; 1 -4 1; 0 1 0];     % Laplacian operator
  F     = make_imfilter_mat(L, sz, 'replicate', 'same');

  % Replicate operator for u and v
  M     = [F, sparse(npixels, npixels);
           sparse(npixels, npixels), F];
       M = this.lambda*M;

   
  % Data term  
  [Ixx, Ixy] = imgGrad(Ix);
  [Iyx, Iyy] = imgGrad(Iy);
  
  Ix2 = Ix.^2;
  Iy2 = Iy.^2;
  IxIy =Ix.*Iy;
  Itx = It.*Ix;
  Ity = It.*Iy;

  
  % Laplacian diffusion term
  
  laplace = Ixx+Iyy;
  IxLaplace = Ix.*laplace;
  IyLaplace = Iy.*laplace;
  
 
%   Perform linearization - note the change in It
  It = It + Ix.*repmat(duv(:,:,1), [1 1 size(It,3)]) ...
          + Iy.*repmat(duv(:,:,2), [1 1 size(It,3)]);
      
      
%Computing vector field derivatives on the other direction of every componenet
[xxv dummy1] = imgGrad(uv(:,:,2));
[dummy2 yyu] = imgGrad(uv(:,:,1));

%Turbulent Schmidt number
Sct=1;

  DtY = abs(xxv)/Sct;
  DtX = abs(yyu)/Sct;
  

  if isa(this.rho_data, 'robust_function')   
       pp_d  = deriv_over_x(this.rho_data,It(:)-laplace(:));
  elseif isa(this.rho_data, 'gsm_density')
      pp_d = -evaluate_log_grad_over_x(this.rho_data, It(:)')';
  else
      error('flow_operator: unknown rho function!');
  end;  
  

  tmp = pp_d.*Ix2(:);
  duu = spdiags(tmp, 0, npixels, npixels);
    tmp = pp_d.*Iy2(:);
  dvv = spdiags(tmp, 0, npixels, npixels);
    tmp = pp_d.*IxIy(:);
  dduv = spdiags(tmp, 0, npixels, npixels);

  A = [duu dduv; dduv dvv] - M;

  % right hand side


      b =   M * uv(:) - [pp_d.*(Itx(:)-(DtX(:).*IxLaplace(:))); pp_d.*(Ity(:)-(DtY(:).*IyLaplace(:)))];
   
  % No auxiliary parameters
  params    = [];
  
  % If the non-linear weights are non-uniform, do more linearization
   
% if(max(pp_su(:)) - min(pp_su(:)) < 1E-6 && ...
%      max(pp_sv(:)) - min(pp_sv(:)) < 1E-6 && ...
if(      max(pp_d(:)) - min(pp_d(:)) < 1E-6)
    iterative = false;
  else
    iterative = true;
     
end