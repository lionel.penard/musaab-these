%This script writes a BAL (Bundle Adjustement in the Large) file format
%necessary for the BA using CERES, provided also in the codes

%% loading the 3D points, camera poses and 2Dtracks of points through the sequence
close all;
clear all;
load('C:\Users\mohammed\Documents\Camera\Lionelstanding\camPoses.mat');
load('C:\Users\mohammed\Documents\Camera\Lionelstanding\tracks.mat');
load('C:\Users\mohammed\Documents\Camera\Lionelstanding\xyzPoints.mat');

%% initial values;
fileID = fopen('Lionelstanding3.txt','w');
%Writing number of views
fprintf(fileID,strcat(num2str(size(camPoses,1)),'\t'));
%Writing number of tracks
fprintf(fileID,strcat(num2str(size(tracks,2)),'\t'));
%hard coding the number of observations, it is the value of the variable
%counter in the next loop
fprintf(fileID,'22924\t');% number of observations
%Hard coding the initial values for f and the prinicipal point
fprintf(fileID,'1300\t');
fprintf(fileID,'960\t');
fprintf(fileID,'540');

%% %iterating and writing the 2D observations (x and y coordinates of a point)
%as viewed by two views
counter = 0;

for i=1:size(tracks,2)
   for j = 1:size(tracks(i).ViewIds,2)
          fprintf(fileID,'\n');
        fprintf(fileID,'%6d  \t',j-1);
        fprintf(fileID,'%6d \t',i-1);
        fprintf(fileID,'%4.13f  \t',tracks(i).Points(j,1));
        fprintf(fileID,'%4.13f',tracks(i).Points(j,2));
%to count the total observations       
counter = counter+1;
   end
end

%Put a break point above to get the value of counter and then hard code it in the observations 
%value above
%% Iterating and writing the camera external parameters, rotation is 3
% values in the form of quaternions and 3 for the translation
for i=1:size(camPoses,1)
  %rotations 
dummy = camPoses.Orientation(i);
qu = rotationMatrixToVector(dummy{1,:});
    fprintf(fileID,'\n');
fprintf(fileID,'%2.9f',qu(1,1));
 fprintf(fileID,'\n');
fprintf(fileID,'%2.9f ',qu(1,2));
 fprintf(fileID,'\n');
fprintf(fileID,'%2.9f ',qu(1,3));
%translations
dummy = camPoses.Location(i);
tra = dummy{1,:};
fprintf(fileID,'\n');
fprintf(fileID,'%4.9f ',tra(1,1));
 fprintf(fileID,'\n');
fprintf(fileID,'%4.9f ',tra(1,2));
 fprintf(fileID,'\n');
fprintf(fileID,'%4.9f ',tra(1,3));
end
%% writing the 3D coordinates of every 3D point
for i = 1:size(xyzPoints,1)
    fprintf(fileID,'\n');
   fprintf(fileID,'%4.7f ',xyzPoints(i,1));
   fprintf(fileID,'\n');
   fprintf(fileID,'%4.7f ',xyzPoints(i,2));
   fprintf(fileID,'\n');
   fprintf(fileID,'%4.7f ',xyzPoints(i,3));
end
%%
fclose(fileID);