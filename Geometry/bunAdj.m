%This is a modified version of matlab bundle adjustement example


%% Structure From Motion From Multiple Views
% Structure from motion (SfM) is the process of estimating the 3-D structure 
% of a scene from a set of 2-D views. It is used in many applications, such 
% as robot navigation, autonomous driving, and augmented reality. This 
% example shows you how to estimate the poses of a calibrated camera from 
% a sequence of views, and reconstruct the 3-D structure of the scene up to
% an unknown scale factor.

% Copyright 2015 The MathWorks, Inc. 

%% Overview
% This example shows how to reconstruct a 3-D scene from a sequence of 2-D
% views taken with a camera calibrated using the
% <matlab:helpview(fullfile(docroot,'toolbox','vision','vision.map'),'visionCameraCalibrator'); Camera Calibrator app>.
% The example uses a |viewSet| object to store and manage the data
% associated with each view, such as the camera pose and the image points,
% as well as matches between points from pairs of views. 
%
% The example uses the pairwise point matches to estimate the camera pose of
% the current view relative to the previous view. It then links the
% pairwise matches into longer point tracks spanning multiple views using
% the |findTracks| method of the |viewSet| object. These tracks then serve
% as inputs to multiview triangulation using the |triangulateMultiview|
% function and the refinement of camera poses and the 3-D scene points
% using the |bundleAdjustment| function.
%
% The example consists of two main parts: camera motion estimation and
% dense scene reconstruction. In the first part, the example estimates the
% camera pose for each view using a sparse set of points matched across the
% views. In the second part, the example iterates over the sequence of
% views again, using |vision.PointTracker| to track a dense set of points
% across the views, to compute a dense 3-D reconstruction of the scene.
%
% The camera motion estimation algorithm consists of the following steps:
%
% # For each pair of consecutive images, find a set of point
% correspondences. This example detects the interest points using the
% |detectSURFFeatures| function, extracts the feature descriptors using the
% |extractFeatures| functions, and finds the matches using the
% |matchFeatures| function. Alternatively, you can track the points across
% the views using |vision.PointTracker|.
% # Estimate the relative pose of the current view, which is the camera
% orientation and location relative to the previous view. The example uses
% a helper function |helperEstimateRelativePose|, which calls
% |estimateFundamentalMatrix| and |cameraPose|.
% # Transform the relative pose of the current view into the coordinate
% system of the first view of the sequence.
% # Store the current view attributes: the camera pose and the image
% points.
% # Store the inlier matches between the previous and the current view.
% # Find point tracks across all the views processed so far.
% # Use the |triangulateMultiview| function to compute the initial 3-D
% locations corresponding to the tracks.
% # Use the |bundleAdjustment| function to refine the camera poses and the
% 3-D points.
% Store the refined camera poses in the |viewSet| object.

%% Read the Input Image Sequence
% Read and display the image sequence.

% Use the |imageSet| object to get a list of all image file names in a
% directory.

% imageDir = fullfile(toolboxdir('vision'), 'visiondata', ...
%       'structureFromMotion');

% function [camPoses xyzPoints reprojectionErrors] = bunAdj(A,Features)
function [camPoses xyzPoints tracks] = bunAdj(A)

imageDir = fullfile('C:\Users\mohammed\Documents\Camera\Lionelstanding');
% 
imSet = imageSet(imageDir);
cameraParams = cameraParameters('IntrinsicMatrix',[A(1,1) 0 0;0 A(2,2) 0; A(1,3) A(2,3) 1]);
% Display the images.
% figure
% montage(imSet.ImageLocation, 'Size', [3, 2]);

% Convert the images to grayscale.
images = cell(1, imSet.Count);
for i = 1:imSet.Count
    I = read(imSet, i);
    images{i} = rgb2gray(I);
end

% title('Input Image Sequence');


%% Create a View Set Containing the First View
% Use a |viewSet| object to store and manage the image points and the
% camera pose associated with each view, as well as point matches between
% pairs of views. Once you populate a |viewSet| object, you can use it to
% find point tracks across multiple views and retrieve the camera poses to
% be used by |triangulateMultiview| and |bundleAdjustment| functions.

% Undistort the first image.
I = images{1}; 
% totMask = mask(:,:,1);
% temp = I;
% temp(totMask) = 0;
% 
% Detect features. Increasing 'NumOctaves' helps detect large-scale
% features in high-resolution images. Use an ROI to eliminate spurious
% features around the edges of the image.
border = 50;
roi = [border, border, size(I, 2)- 2*border, size(I, 1)- 2*border];
prevPoints   = detectSURFFeatures(I);

% Extract features. Using 'Upright' features improves matching, as long as
% the camera motion involves little or no in-plane rotation.
prevFeatures = extractFeatures(I, prevPoints,'SURFSize',128);

% Create an empty viewSet object to manage the data associated with each
% view.



%% Add the Rest of the Views
% Go through the rest of the images. For each image
%
% # Match points between the previous and the current image.
% # Estimate the camera pose of the current view relative to the previous
%   view.
% # Compute the camera pose of the current view in the global coordinate 
%   system relative to the first view.
% # Triangulate the initial 3-D world points.
% # Use bundle adjustment to refine all camera poses and the 3-D world
%   points.
vSet = viewSet;
viewId = 1;
% vSet = addView(vSet, viewId, 'Points', prevPoints, 'Orientation', eye(3),...
%     'Location', [0 0 0]);
% vSet = addView(vSet, viewId, 'Points', Features(1).pointsA, 'Orientation', eye(3),...
%     'Location', [0 0 0]);
vSet = addView(vSet, viewId, 'Points', prevPoints, 'Orientation', ...
    eye(3, 'like', prevPoints.Location), 'Location', ...
    zeros(1, 3, 'like', prevPoints.Location));


% for i=1:size(Features,1)
 for i = 2:numel(images)  
    
     I = images{i};
%      totMask = mask(:,:,i);
% temp = I;
% temp(totMask) = 0;
    % Detect, extract and match features.
%     currPoints   = Features(i).pointsB;
%      prevPoints   = Features(i).pointsA;
%     indexPairs = Features(i).index;

    currPoints   = detectSURFFeatures(I);
    currFeatures = extractFeatures(I, currPoints,'SURFSize',128);    
    indexPairs = matchFeatures(prevFeatures, currFeatures, ...
        'MaxRatio', .5,'Unique',  true);
    
    % Select matched points.
    matchedPoints1 = prevPoints(indexPairs(:, 1));
    matchedPoints2 = currPoints(indexPairs(:, 2));
    
    % Estimate the camera pose of current view relative to the previous view.
    % The pose is computed up to scale, meaning that the distance between
    % the cameras in the previous view and the current view is set to 1.
    % This will be corrected by the bundle adjustment.
    [relativeOrient, relativeLoc, inlierIdx] = helperEstimateRelativePose(...
        matchedPoints1, matchedPoints2, cameraParams);
    
    % Add the current view to the view set.
%     vSet = addView(vSet, i+1, 'Points', currPoints);
vSet = addView(vSet, i, 'Points', currPoints);
    
    % Store the point matches between the previous and the current views.
%     vSet = addConnection(vSet, i, i+1, 'Matches', indexPairs(inlierIdx,:));
vSet = addConnection(vSet, i-1, i, 'Matches', indexPairs(inlierIdx,:));

    
    % Get the table containing the previous camera pose.
%     prevPose = poses(vSet, i);
%     prevOrientation = prevPose.Orientation{1};
%     prevLocation    = prevPose.Location{1};
         prevPose = poses(vSet, i-1);
    prevOrientation = prevPose.Orientation{1};
    prevLocation    = prevPose.Location{1};
    % Compute the current camera pose in the global coordinate system 
    % relative to the first view.
%     orientation = prevOrientation * relativeOrient;
%     location    = prevLocation + relativeLoc * prevOrientation;
%     vSet = updateView(vSet, i+1, 'Orientation', orientation, ...
%         'Location', location);
orientation = relativeOrient * prevOrientation;
    location    = prevLocation + relativeLoc * prevOrientation;
    vSet = updateView(vSet, i, 'Orientation', orientation, ...
        'Location', location);
    
    % Find point tracks across all views.
    tracks = findTracks(vSet);

    % Get the table containing camera poses for all views.
    camPoses = poses(vSet);

    % Triangulate initial locations for the 3-D world points.
    [xyzPoints errors] = triangulateMultiview(tracks, camPoses, cameraParams);
    
    % Refine the 3-D world points and camera poses.
%     [xyzPoints, camPoses, reprojectionErrors, MatchedPoints] = bundleAdjustment(xyzPoints, ...
%         tracks, camPoses, cameraParams, 'FixedViewId', 1, ...
%         'PointsUndistorted', false);
% measurements  = [measurements;struct('points',MatchedPoints)];
    % Store the refined camera poses.
    vSet = updateView(vSet, camPoses);

    prevFeatures = currFeatures;
    prevPoints   = currPoints;  
    i
 end
 
 %Filter the results depending on the plausible values in the depth
 %componant
z= xyzPoints(:,3);
idx = errors < 5 & z > 0 & z < 900;
xyzPoints = xyzPoints(idx, :);
tracks = tracks(idx);


%% References
%
% [1] M.I.A. Lourakis and A.A. Argyros (2009). "SBA: A Software Package for
%     Generic Sparse Bundle Adjustment". ACM Transactions on Mathematical
%     Software (ACM) 36 (1): 1-30.
%
% [2] R. Hartley, A. Zisserman, "Multiple View Geometry in Computer
%     Vision," Cambridge University Press, 2003.
%
% [3] B. Triggs; P. McLauchlan; R. Hartley; A. Fitzgibbon (1999). "Bundle
%     Adjustment: A Modern Synthesis". Proceedings of the International
%     Workshop on Vision Algorithms. Springer-Verlag. pp. 298-372.

displayEndOfDemoMessage(mfilename)
end
