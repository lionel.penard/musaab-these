function [camPosesOpt] = ReadOptimisedCamPoses(optimisedPoses)
sz = size(optimisedPoses,1);
PosesSZ = sz/6;
ViewId=[];
Orientation=cell(PosesSZ,1);
Location=cell(PosesSZ,1);
counter = 1;
for i = 1:6:sz

        temp = [optimisedPoses(i,1),optimisedPoses(i+1,1),optimisedPoses(i+2,1)];
        ViewId = [ViewId;counter];
        temp2 = {[optimisedPoses(i+3,1),optimisedPoses(i+4,1),optimisedPoses(i+5,1)]};
        Location(counter,1) = temp2;
        temp3 = {rotationVectorToMatrix(temp)};
          Orientation(counter,1) = temp3; 
          counter = counter+1;
end
ViewId = uint32(ViewId);
camPosesOpt = table(ViewId,Orientation,Location);
