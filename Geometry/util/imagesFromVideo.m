workingDir = tempname;
mkdir(workingDir)
mkdir(workingDir,'images')
arcVideo = VideoReader('C:\Users\mohammed\Videos\CanalInclinable\CalibPattern.mp4');

ii = 1;
while hasFrame(arcVideo)
img = readFrame(arcVideo);
filename = [sprintf('%03d',ii) '.jpg'];
fullname = fullfile(workingDir,'images',filename);
imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
ii = ii+1;
end