function [solutions] = sturm(Fs,PP)
%This function accepts the fundamental matrices between images and the
%initial principle point to compute many initial solutions to the focal
%length based on sturm method

k = [1 0 PP(1);0 1 PP(2);0 0 1];
% the ad-hoc sturm fZero large focal length value as advised in sturm
% paper. 
fZero=9000;
solutions = [];
for i=1:size(Fs,3)
for j=i+1:size(Fs,4)
G = k'*Fs(:,:,i,j)*k;
G2 = diag([fZero;fZero;1])*G*diag([fZero;fZero;1]);
fro = norm(G2,'fro');
scaledG2 =G2./fro;
[U S V ] = svd(scaledG2);
l = S(1,1)^2*((1-U(3,1)^2)*(1-V(3,1)^2))-S(2,2)^2*((1-U(3,2)^2)*(1-V(3,2)^2));
m = S(1,1)^2*(U(3,1)^2+V(3,1)^2-2*(U(3,1)^2*V(3,1)^2))-S(2,2)^2*(U(3,2)^2+V(3,2)^2-2*(U(3,2)^2*V(3,2)^2));
n = S(1,1)^2*(U(3,1)^2*V(3,1)^2-S(2,2)^2*(U(3,2)^2*V(3,2)^2));
sols = roots([l m n]);
sols = sqrt(sols(sols>0));
sols = sols(~imag(sols));
solutions = [solutions;sols(sols>0)*fZero];

x = S(1,1)*U(3,1)*U(3,2)*((1-V(3,1)^2))+S(2,2)*V(3,1)*V(3,2)*((1-U(3,2)));
y = U(3,2)*V(3,1)*(S(1,1)*U(3,1)*V(3,1)+S(2,2)*U(3,2)*V(3,2));

x1 = S(1,1)*V(3,1)*V(3,2)*((1-U(3,1)^2))+S(2,2)*U(3,1)*U(3,2)*((1-V(3,2)));
y1 = U(3,1)*V(3,2)*(S(1,1)*U(3,1)*V(3,1)+S(2,2)*U(3,2)*V(3,2));

sols = -(y-y1)/(x-x1);
sols = sols(~imag(sols));
solutions = [solutions;sols(sols>0)*fZero];

end
end

%Filtering solutions by excluding those between two thresholds
solutions = solutions(solutions>500);
solutions = solutions(solutions<10000);

