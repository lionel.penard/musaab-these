function [Fs,masks]= computeF(N)
%This function computes the fundamental between EVERY pair of images in the
%sequence, the feature points are extracted after masking the river area 
%manually. The first iteration saves the masks so they could be
%automatically used on the iterations that comes after 
close all;
for i=1: N-1
    j=i;
    close all;
    for j=j+1:N
    imgA = imread(strcat('C:\Users\mohammed\Documents\Camera\Aulnay\',num2str(i+10),'.jpg'));
    imgB = imread(strcat('C:\Users\mohammed\Documents\Camera\Aulnay\',num2str(j+10),'.jpg'));
imgA = rgb2gray(imgA);
imgB = rgb2gray(imgB);
%% First iteration, the first image with all other images. 
if i ==1 && j==2
totMask = Roi(1,imgA);
masks(:,:,1)=totMask;
end

if i==1  
  temp = imgA;
  totMask=masks(:,:,1);
temp(totMask) = 0;
pointsA = detectSURFFeatures(temp);  
totMask = Roi(1,imgB);
temp = imgB;
temp(totMask) = 0;
pointsB = detectSURFFeatures(temp);
masks(:,:,j)=totMask;
[featuresA, pointsA] = extractFeatures(imgA, pointsA,'SURFSize',128);
[featuresB, pointsB] = extractFeatures(imgB, pointsB,'SURFSize',128);
% use options in matchFeatures functions to control the quality of detected
% features
 indexPairs = matchFeatures(featuresA, featuresB, 'MaxRatio', .7, 'Unique',  true);
pointsA = pointsA(indexPairs(:,1));
pointsB = pointsB(indexPairs(:,2));
%use options in estimateFundamentalMatrix function to control the
%robustness of the results
F = estimateFundamentalMatrix(pointsA,pointsB,'Method','RANSAC','NumTrials',8000,'DistanceThreshold',1e-1);
Fs(:,:,i,j) = F;
Fs(:,:,j,i) = F';
else
%% other iterations
    close all;
    totMask = masks(:,:,i);
temp = imgA;
temp(totMask) = 0;
pointsA = detectSURFFeatures(temp);
totMask = masks(:,:,j);
temp = imgB;
temp(totMask) = 0;
pointsB = detectSURFFeatures(temp);
% figure; imshow(imgA); hold on;
% plot(pointsA);
% title('Corners in A');
% figure; imshow(imgB); hold on;
% plot(pointsB);
% title('Corners in B');
[featuresA, pointsA] = extractFeatures(imgA, pointsA,'SURFSize',128);
[featuresB, pointsB] = extractFeatures(imgB, pointsB,'SURFSize',128);
indexPairs = matchFeatures(featuresA, featuresB, 'MaxRatio', .7, 'Unique',  true);
pointsA = pointsA(indexPairs(:,1));
pointsB = pointsB(indexPairs(:,2));
figure; showMatchedFeatures(imgA, imgB, pointsA, pointsB);
legend('A', 'B');

i
j
F = estimateFundamentalMatrix(pointsA,pointsB,'Method','RANSAC','NumTrials',8000,'DistanceThreshold',1e-1);
Fs(:,:,i,j) = F;
Fs(:,:,j,i) = F';
end
    end
end
end

