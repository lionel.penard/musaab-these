function velocity = CrossSectionCalc(theta,k,uvSeq,im,SceneScale,VisualizationScale,time,ptCloud,NuPoints)
close all;
% This function accepts the parameters necessary to compute the plane
% equations: theta,k,im. It also accepts the 3D scene file ptCloud to use it
% for 3D vector plotting of the velocity. It also accepts the mean 2d
% displacement uvSeq, the time between images time, the desired number of 
% points on the cross-section, the scene scale ScenScale and a Visualization
% scale VisualizationScale that is used to scale the vector field in the 
% main direction for visualization purposes.

% The function computes the necessary plane equations , shows the image and
% let the user define the points on the cross section. Using the
% displacement vector field, a similar set of points is obtained as the
% result of the displacement of the original points. All points are then
% projected back to 3D and scaled for visulaization. The function then
% scale again the points to the scene scale and compute the velocity using
% the time between frames. The velocity is plotted against the distance of
% every point to the first point in the cross-section

%% preparing the river plane equation vector
PointDistance=0;

T = [1 0 0 0;0 sin(theta) cos(theta) 0;0 -cos(theta) sin(theta) 2;0 0 0 1];
Pi_c= T'*[0 ; 0; -1;0];
N_gnd_c = Pi_c(1:3);
N_gnd_c=N_gnd_c/norm(N_gnd_c);
D_gnd_c=Pi_c(4);


%% Define the starting and ending points of the trajectory, then automatically define equi-distance points on the line
imshow(im);
hold on;


lineXY = ginput(2);
stepLengths = sqrt(sum(diff(lineXY,[],1).^2,2));
stepLengths = [0; stepLengths] % add the starting point;
cumulativeLen = cumsum(stepLengths);
finalStepLocs = linspace(0,cumulativeLen(end), NuPoints);
finalLineXY = interp1(cumulativeLen, lineXY, finalStepLocs);
x=round(finalLineXY(:,1));
y=round(finalLineXY(:,2));
plot(x,y,'r.');
hold off;

%% Plot the 3D point cloud, then project and scale the 2D displacement into it. 
velocity=[];
velocity2=[];
velocity2D=[];

pcshow(ptCloud);
hold on;
counter=1;
for j=1:1:size(x)    
ray=(inv(k)*[x(j); y(j); 1]);
lambda=-D_gnd_c./(N_gnd_c'*ray);
point = lambda'*ray;
point = [point; 1];
point = (T*point);

if j==1
    FirstPointOnTheCross=point;
else
    PointDistance= [PointDistance;norm(point-FirstPointOnTheCross)];
end

uv=mean(uvSeq,4);
u=uv(:,:,1);
v=uv(:,:,2);
tempU=u(y(j),x(j));
xDisplaced=tempU+x(j);
xDisplacedScaled=VisualizationScale*(tempU)+x(j);
tempV=v(y(j),x(j));
yDisplaced=tempV+y(j);
yDisplacedScaled=VisualizationScale*(tempV)+y(j);
 velocity2D(:,counter)=tempV;
ray=(inv(k)*[xDisplaced; yDisplaced; 1]);
lambda=-D_gnd_c./(N_gnd_c'*ray);
point1 = lambda'*ray;
point1 = [point1; 1];
point1 = T*point1;

ray=(inv(k)*[xDisplacedScaled; yDisplacedScaled; 1]);
lambda=-D_gnd_c./(N_gnd_c'*ray);
pointScaled = lambda'*ray;
pointScaled = [pointScaled; 1];
pointScaled = T*pointScaled;
d=norm(point-pointScaled);
 plot3(point(1) ,point(2),point(3) ,'r.');
 arrow(point(1:3)',pointScaled(1:3)',d,'EdgeColor','r','tipangle',135,'baseangle',20,'width',0.1,'FaceColor','r');%change d to change arrow head
 plot3(pointScaled(1) ,pointScaled(2),pointScaled(3) ,'r^');
 
 % This time scale the displacement with the true scale of the scene and
 % compute the velocity.
displ = norm(point-point1);
displ=displ*SceneScale;
 velocity(:,counter)=displ/time;
 displ = norm(point-point1);
 displ=displ*2.7;
  velocity2(:,counter)=displ/time;
 counter=counter+1;
end
hold off;
%% plot the velocity against the the distance from the first point.
figure;
plot(SceneScale.*PointDistance,velocity);
figure;
plot(2.7.*PointDistance,velocity2);
end