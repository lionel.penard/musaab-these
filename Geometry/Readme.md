These are the codes that work on the geometrical problem. The folder Util contains codes that do general stuff like extracting the images from a video etc...
The code is in Matlab and it depends heavily on matlab computer vision toolbox.

Autocalibration:

- Compute the fundamental matrices between frames --> computeF.m
- Use Sturm method to compute initial focal length value --> sturm.m
- Use modified Matlab bundle adjustement code just to get the initial camera poses and 3D points --> bunAdj.m.
- Use cameraPoses, Tracks and 3D points saved from previous step to write a .BAL file to be used in Ceres bundle adjustement --> WriteBal.m.
- Run Bundle Adjustement C++ code on the Bal file. The code prints out the optimized focal and principal point.


3D reconstruction:
use Pop3D.m to build a 3D model as specified in my thesis. To calculate metric velocity on a cross-section use CrossSectionCalc.m