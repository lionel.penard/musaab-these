function [N_gnd_c D_gnd_c] = Pop3D(im,k,theta,h,Offset)
%This functions accepts an image im, camera intrinsics matrix k and the
%angle theta, distance of the camera from river surface h and an offset variable 
% if the image is too big to pop up a 3D model of the scene of two orthogonal planes.


%% Manually segment the river plane, the orthogonal plane will be the one adjacent to the first line segment.
[mask,xi,yi] = roipoly(im);
[i,j]=find(mask);
points=[];
colors=[];
point1=0;
point2=0;
R = im(:,:,1);
G = im(:,:,2);
B = im(:,:,3);

%% Now compute the plane vector in camera coordiantes system

%define the external camera parameters
     T = [1 0 0 0;0 sin(theta) cos(theta) 0;0 -cos(theta) sin(theta) h;0 0 0 1];

      Pi_c= T'*[0 ; 0; -1;0]
      N_gnd_c = Pi_c(1:3);
      N_gnd_c=N_gnd_c/norm(N_gnd_c);
      D_gnd_c=Pi_c(4);
      
%% Iterating over river pixels and project them back to 3D (camera coordinates still)     
for PixelIndex=1:Offset:size(i)
%     undistortedPoint = undistortPoints([j(PixelIndex),i(PixelIndex)],k);
    undistortedPoint =[j(PixelIndex),i(PixelIndex)];
    ray=(inv(k.IntrinsicMatrix')*[undistortedPoint(1);undistortedPoint(2); 1]);
    lambda=-D_gnd_c./(N_gnd_c'*ray);
      point = lambda'*ray;
        point = [point; 1];
        %Transform point to world coordinates system.
          point = T*point;
      points = [point';points];
      color = [R(i(PixelIndex),j(PixelIndex)),G(i(PixelIndex),j(PixelIndex)),B(i(PixelIndex),j(PixelIndex))];
      colors=[color;colors];
end
%% compute the orthogonal plane vector using the two points of thre first segmented line(camera coordinates still)
%        undistortedPoint = undistortPoints([xi(1),yi(1)],k);
         undistortedPoint = [xi(1),yi(1)];
       ray=(inv(k.IntrinsicMatrix')*[undistortedPoint(1);undistortedPoint(2);1]);
       lambda=-D_gnd_c/(N_gnd_c'*ray);
       point1 = lambda'*ray; 
       
       undistortedPoint = undistortPoints([xi(2),yi(2)],k);
       ray=(inv(k.IntrinsicMatrix')*[undistortedPoint(1);undistortedPoint(2);1]);
       lambda=-D_gnd_c/(N_gnd_c'*ray);
       point2 = lambda'*ray;
       
       %compute and test if edge points projects to world frame with z = 0 P_w=inv(T)*P_c;
       point3=[point1; 1];
       point3=T*point3;
       point3=point3(1:3,:)
       point4=[point2; 1];
       point4=T*point4;
       point4= point4(1:3,:)

     
        vec = point2-point1;
        N_wall_c = cross(N_gnd_c,vec);
        N_wall_c=N_wall_c/norm(N_wall_c);
        D_wall_c = dot(-N_wall_c,point1);


%% Reconstruct the orthogonal plane
mask = roipoly(im,[xi(1),xi(2),xi(2),xi(1)],[yi(1),yi(2),50,50]);

%Or uncomment the following to do manual segmentation.
% [mask,xi,yi] = roipoly(im);

[i,j]=find(mask);
for PixelIndex=1:Offset:size(i)  
    undistortedPoint = undistortPoints([j(PixelIndex),i(PixelIndex)],k);
    undistortedPoint = [j(PixelIndex),i(PixelIndex)];
    ray=(inv(k.IntrinsicMatrix')*[undistortedPoint(1);undistortedPoint(2); 1]);
    lambda=-D_wall_c./(N_wall_c'*ray);
      point = lambda'*ray;
            point = [point; 1];
            %transform point to world coordinates system
             point = T*point;
      points = [point';points];
      color = [R(i(PixelIndex),j(PixelIndex)),G(i(PixelIndex),j(PixelIndex)),B(i(PixelIndex),j(PixelIndex))];
      colors=[color;colors];
end

%%Building and plotting the point cloud
points=points(:,1:3);
ptCloud= pointCloud(points,'color',colors);
pcshow(ptCloud);
hold on;
axis equal;
% pcwrite(ptCloud,'testt','PLYFormat','binary');
end